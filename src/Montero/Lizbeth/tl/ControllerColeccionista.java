package Montero.Lizbeth.tl;

import Montero.Lizbeth.bl.categoria.Categoria;
import Montero.Lizbeth.bl.coleccionista.Coleccionista;
import Montero.Lizbeth.bl.coleccionista.IColeccionistaDAO;
import Montero.Lizbeth.bl.coleccionista.SqlServerColeccionista;
import Montero.Lizbeth.dao.DAOFactory;
import Montero.Lizbeth.utils.Utils;
import javafx.scene.control.DatePicker;

import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;

public class ControllerColeccionista {
    private IColeccionistaDAO dao;// encapsulamiento
    private DAOFactory factory;

    public ControllerColeccionista() throws Exception {
        factory = DAOFactory.getDAOFactory(0);
        dao = factory.getColeccionista();
    }

   public String registrarColeccionista(String identificacion, String tipo, String nombre, String avatar, LocalDate fechaNacimiento,int edad, String userName,  String correo, String contrasenia, String estado, double calificacion, String direccion, int esModerador) throws Exception {
       Coleccionista c = new Coleccionista(  identificacion,  tipo,  nombre,  avatar,  fechaNacimiento, edad,  userName,   correo,  contrasenia,  estado,  calificacion,  direccion,  esModerador);

       return dao.registrarColeccionista(c);}
        /*try {
            factory = DAOFactory.getDAOFactory(Utils.getMotorBD());
            dao = (IColeccionistaDAO) factory.getColeccionista();

            return dao.registrarColeccionista(c);
        } catch (SQLException e) {
            return e.getMessage();
        } catch (ClassNotFoundException e) {
            return e.getMessage();
        } catch (Exception e) {
            return e.getMessage();
        }
    }*/

  public ArrayList<String> listarColeccionistas() throws Exception {
        return   dao.listarColeccionistas();
    }

//TODO REVISAR QUE FUNCIONE CORRECTAMENTE

    public String buscarColeccionista(String userName) throws Exception {
        Coleccionista c = dao.buscarColeccionista(userName);
        if (c != null) {
            return c.toString();
        }
        return "El userName no existe";
    }


    public String modificarColeccionista(String identificacion, String tipo, String nombre, String avatar, LocalDate fechaNacimiento,int edad, String userName,  String correo, String contrasenia, String estado, double calificacion, String direccion, int esModerador) throws Exception {
        Coleccionista cat = new Coleccionista(  identificacion,  tipo,  nombre,  avatar,  fechaNacimiento, edad,  userName,   correo,  contrasenia,  estado,  calificacion,  direccion,  esModerador);
        return dao.modificarColeccionista(cat);
    }

    public String eliminarColeccionista(String identificacion) throws Exception {
        Coleccionista c = dao.buscarColeccionista(identificacion);
        if (c != null) {
            return dao.eliminarColeccionista(identificacion);
        }
        return "La identificacion del coleccionista  no existe";
    }
}
