package Montero.Lizbeth.tl;

import Montero.Lizbeth.bl.administrador.Administrador;
import Montero.Lizbeth.bl.administrador.IAdministradorDAO;
import Montero.Lizbeth.bl.administrador.SqlServerAdministrador;
import Montero.Lizbeth.bl.coleccionista.Coleccionista;
import Montero.Lizbeth.dao.DAOFactory;
import Montero.Lizbeth.utils.Utils;

import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class ControllerAdministrador {
    private IAdministradorDAO dao;// encapsulamiento
    private DAOFactory factory;
    //private ArrayList<Categoria> categorias;


    public ControllerAdministrador() throws Exception {
        factory = DAOFactory.getDAOFactory(0);
        dao = factory.getAdministrador();
    }


    // el controller hace el amarre, por eso no se utilizan los if

    public String registrarAdmi(String identificacion, String tipo, String nombre, String avatar, LocalDate fechanacimiento, int edad, String username, String correo, String password, String estado   ) {
        Administrador c = new Administrador(  identificacion,  tipo,  nombre,  avatar,  fechanacimiento,  edad,  username,  correo,  password,  estado);
        try {
            factory = DAOFactory.getDAOFactory(Utils.getMotorBD());// encapsulamiento
            dao = (IAdministradorDAO) factory.getAdministrador();

            return dao.insertar(c);
        } catch (SQLException e) {
            return e.getMessage();
        } catch (ClassNotFoundException e) {
            return e.getMessage();
        } catch (Exception e) {
            return e.getMessage();
        }
    }

    public ArrayList<String> listarAdministrador() throws Exception {
        return   dao.listar();
    }

    public String buscarAdministrador(String userName) throws Exception {
        Administrador c = dao.buscarAdministrador(userName);
        if (c != null) {
            return c.toString();
        }
        return "El userName no existe";
    }





}
