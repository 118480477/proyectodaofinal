package Montero.Lizbeth.tl;


import Montero.Lizbeth.bl.item.Item;
import Montero.Lizbeth.bl.subasta.Subasta;
import Montero.Lizbeth.bl.vendedor.IVendedorDAO;
import Montero.Lizbeth.bl.vendedor.SqlServerVendedor;
import Montero.Lizbeth.bl.vendedor.Vendedor;
import Montero.Lizbeth.dao.DAOFactory;
import Montero.Lizbeth.utils.Utils;

import java.sql.SQLException;
import java.util.ArrayList;

public class ControllerVendedor {

    private IVendedorDAO dao;// encapsulamiento
    private DAOFactory factory;
    private ArrayList<String> vendedores;


    public ControllerVendedor() throws Exception {
        factory = DAOFactory.getDAOFactory(0);
        dao = factory.getVendedor();
    }


    // el controller hace el amarre, por eso no se utilizan los if
    public String registrarVendedores(String identificacion, String nombre,String apellido, String correo,String direccion, double puntuacion) {
        Vendedor  v = new Vendedor(identificacion,nombre,apellido,correo,direccion,puntuacion);
        try {
            factory = DAOFactory.getDAOFactory(Utils.getMotorBD());// encapsulamiento
            dao = (IVendedorDAO) factory.getVendedor();

            return dao.insertarVendedor(v);
        } catch (SQLException e) {
            return e.getMessage();
        } catch (ClassNotFoundException e) {
            return e.getMessage();
        } catch (Exception e) {
            return e.getMessage();
        }
    }

    public ArrayList<String> listarVendedor() throws Exception {
        return   dao.listarVendedor();
    }
    public String modificarVendedor( String identificacion, String nombre,String apellido, String correo,String direccion, double puntuacion) throws Exception {
        Vendedor v= new Vendedor(identificacion,nombre,apellido,correo,direccion,puntuacion);
        return dao.modificarVendedro(v);
    }

    public String buscarVendedor(String identificacion) throws Exception {
        Vendedor v = dao.buscarVendedor(identificacion);
        if (v != null) {
            return v.toString();
        }
        return  "La identificacion del vendeor  no existe";
    }


    public String buscarItem(String codigo) throws Exception {
        Item s = dao.buscarItem(codigo);
        if (s != null) {
            return s.toString();
        }
        return "El código del objeto   no existe";
    }


    public String eliminarVendedor(String identificacion) throws Exception {
        Vendedor v =dao.buscarVendedor(identificacion);
        if (v != null) {
            return dao.eliminarVendedor(identificacion);
        }
        return "La identificacion del vendeor  no existe";
    }
}

