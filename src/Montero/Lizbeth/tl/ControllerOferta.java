package Montero.Lizbeth.tl;

import Montero.Lizbeth.bl.categoria.Categoria;
import Montero.Lizbeth.bl.coleccionista.Coleccionista;
import Montero.Lizbeth.bl.item.IItemDAO;
import Montero.Lizbeth.bl.item.Item;
import Montero.Lizbeth.bl.oferta.IOfertaDAO;
import Montero.Lizbeth.bl.oferta.Oferta;
import Montero.Lizbeth.bl.subasta.Subasta;
import Montero.Lizbeth.dao.DAOFactory;

import java.time.LocalDate;
import java.util.ArrayList;

public class ControllerOferta {
    private IOfertaDAO dao;// encapsulamiento
    private DAOFactory factory;
    private ArrayList<Oferta> oferta;


    public ControllerOferta() throws Exception {
        factory = DAOFactory.getDAOFactory(0);
        dao = factory.getOferta();
    }
/*
    public String registrarOferta(String codigo, Double precioOfertado, LocalDate fechaOferta, String userName, String subasta_codigo) throws Exception {
        Oferta o = new Oferta(
                codigo,  precioOfertado,  fechaOferta,  userName,  subasta_codigo);

        return dao.insertar(o);}



    public ArrayList<String> listarOferta() throws Exception {
        return   dao.listar();
    }


    public void registrarOferta(String codiooferta, String precio, String user, String codSUBASTA) {
        Oferta o= new Oferta(codiooferta,precio,user,codSUBASTA);
    }*/





    public String registrarOferta(String codigo, Double precioOfertado, LocalDate fechaOferta, String userName, String codigosubasta) throws Exception {
        Coleccionista coleccionista = dao.buscarColeccionista(userName);
        Subasta subasta = dao.buscarSubasta(codigosubasta);
        Oferta oferta = new Oferta(codigo, precioOfertado, fechaOferta, coleccionista, subasta);
        return dao.registrarOferta(oferta, coleccionista, subasta);
    }

    public String modificarOferta(String codigo, Double precioOfertado, LocalDate fechaOferta ,String userName, String codigosubasta) throws Exception {
        Coleccionista coleccionista = dao.buscarColeccionista(userName);
        Subasta subasta = dao.buscarSubasta(codigosubasta);
        Oferta oferta = new Oferta(codigo, precioOfertado, fechaOferta, coleccionista, subasta);
        return dao.modificarOferta(oferta, coleccionista, subasta);
    }

    public String eliminarOferta(String codigo) throws Exception {
        Oferta oferta = dao.buscarOferta(codigo);
        if (oferta != null) {
            return dao.eliminarOferta(oferta);
        }
        return "El codigo de la oferta no existe";
    }

    public String buscarOferta(String codigo) throws Exception {
        Oferta oferta = dao.buscarOferta(codigo);
        if (oferta != null) {
            return oferta.toString();
        }
        return "";
    }

    public ArrayList<String> listarOferta() throws Exception {
        return dao.listarOfertas();
    }

}
