package Montero.Lizbeth.tl;

import Montero.Lizbeth.bl.coleccionista.Coleccionista;
import Montero.Lizbeth.bl.subasta.ISubastaDAO;
import Montero.Lizbeth.bl.subasta.SqlServerSubasta;
import Montero.Lizbeth.bl.subasta.Subasta;
import Montero.Lizbeth.dao.DAOFactory;
import Montero.Lizbeth.utils.Utils;

import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;

public class ControllerSubasta {
    private ISubastaDAO dao;// encapsulamiento
    private DAOFactory factory;
    private ArrayList<String> subastas;



    public ControllerSubasta() throws Exception {
        factory = DAOFactory.getDAOFactory(0);
        dao = factory.getSubasta();
    }


    // el controller hace el amarre, por eso no se utilizan los if

  public String registrarSubasta(String codigo, LocalDate fechaCreacion, LocalDate fechaInicio, int horaInicio, LocalDate fechaVencimiento, int horaVencimiento, String estadoSubasta, double precioMinimoInicial,String ordencompra_codigo, String userName_usuario) {
      Subasta s;
      s = new Subasta(codigo, fechaCreacion, fechaInicio, horaInicio, fechaVencimiento, horaVencimiento, estadoSubasta, precioMinimoInicial, ordencompra_codigo, userName_usuario);
      try {
          factory = DAOFactory.getDAOFactory(Utils.getMotorBD());// encapsulamiento
          dao =  factory.getSubasta();

          return dao.insertarSubasta(s);
      } catch (SQLException e) {
          return e.getMessage();
      } catch (ClassNotFoundException e) {
          return e.getMessage();
      } catch (Exception e) {
          return e.getMessage();
      }

  }
//TODO REVISAR QUE FUNCIONE CORRECTAMENTE

    public String buscarSubasta(String codigo) throws Exception {
        Subasta s = dao.buscarSubasta(codigo);
        if (s != null) {
            return s.toString();
        }
        return "El código de la subasta  no existe";
    }

  /* public String registrarSubasta(String codigo, LocalDate fechaCreacion, LocalDate fechaInicio, int horaInicio, LocalDate fechaVencimiento, int horaVencimiento, String estado, double precioBase, String ordencompra_codigo, String userName_usuario) throws Exception {
        Coleccionista coleccionista = dao.buscarColeccionista(userName_usuario);
        Subasta subasta = new Subasta( codigo,  fechaCreacion,  fechaInicio,  horaInicio,  fechaVencimiento,  horaVencimiento,  estado,  precioBase,  ordencompra_codigo,  userName_usuario);
        return dao.registrarSubasta(subasta, coleccionista);
    }*/

    public String modificarSubasta(String codigo, LocalDate fechaCreacion, LocalDate fechaInicio, int horaInicio, LocalDate fechaVencimiento, int horaVencimiento, String estado, double precioBase, String ordencompra_codigo, String userName_usuario) throws Exception {
        Coleccionista coleccionista = dao.buscarColeccionista(userName_usuario);
        Subasta subasta = new Subasta(  codigo,  fechaCreacion,  fechaInicio,  horaInicio,  fechaVencimiento,  horaVencimiento,  estado,  precioBase,  ordencompra_codigo,  userName_usuario);
        return dao.modificarSubasta(subasta, coleccionista);
    }



    public String eliminarSubasta(String codigo) throws Exception {
        Subasta subasta = dao.buscarSubasta(codigo);
        if (subasta != null) {
            return dao.eliminarSubasta(codigo);
        }
        return "El código de la subasta no existe";
    }

    public ArrayList<String> listarSubasta() throws Exception {
        return dao.listarSubasta();
    }

    /*public ArrayList<String> listarSubastasAbiertas() throws Exception {
        return dao.listarSubastasAbiertas();
    }*/

    public ArrayList<String> listarObjetosSubasta(String codigo) throws Exception {
        return dao.listarObjetosSubasta(codigo);
    }

    public double listarMayorOferta(String codigo) throws Exception {
        return dao.listarMayorOferta(codigo);
    }

    public int listarCantidadOfertas(String codigo) throws Exception {
        return dao.listarCantidadOferta(codigo);
    }

    public ArrayList<String> listarOfertasSubasta(String codigo) throws Exception {
        return dao.listarOfertasSubasta(codigo);
    }

    public String asociarObjetosSubasta(String cod_subasta, String codObjeto) throws Exception {
        return dao.asociarObjetosSubasta(cod_subasta, codObjeto);
    }


    public String asociarOfertasSubasta(String codigo, Double precioOfertado, LocalDate fechaOferta , String coleccionista, String subasta) throws Exception {

        return dao.asociarOfertaSubasta(codigo, precioOfertado, fechaOferta, coleccionista, subasta);
    }
}
