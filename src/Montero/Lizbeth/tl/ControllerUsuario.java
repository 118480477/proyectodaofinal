package Montero.Lizbeth.tl;

import Montero.Lizbeth.bl.usuario.IUsuarioDAO;
import Montero.Lizbeth.bl.usuario.SqlServerUsuario;
import Montero.Lizbeth.bl.usuario.Usuario;
import Montero.Lizbeth.dao.DAOFactory;
import Montero.Lizbeth.utils.Utils;

import java.sql.SQLException;
import java.util.ArrayList;

public class ControllerUsuario {

    private IUsuarioDAO dao;// encapsulamiento
    private DAOFactory factory;

    public ControllerUsuario() throws Exception {
        factory = DAOFactory.getDAOFactory(0);
        dao = factory.getUsuario();
    }

    // el controller hace el amarre, por eso no se utilizan los if

    public String registrarUsuario( String nombre, String userName, String password, String tipo) {
        Usuario u= new Usuario( nombre,userName,password,tipo);
        try {
            factory = DAOFactory.getDAOFactory(Utils.getMotorBD());// encapsulamiento
            dao = (IUsuarioDAO) factory.getUsuario();
            return dao.insertarUsuario(u);
        } catch (SQLException e) {
            return e.getMessage();
        } catch (ClassNotFoundException e) {
            return e.getMessage();
        } catch (Exception e) {
            return e.getMessage();
        }
    }

    public String[] listarUsuario() {
        String[] data;
        SqlServerUsuario usuarioDao = new SqlServerUsuario();
        int i = 0;
        try {
            ArrayList<String> listaUsuario = usuarioDao.listarUsuario();
            data = new String[listaUsuario.size()];
            for (String s : listaUsuario) {
                data[i] = s.toString();
                i++;
            }
            return data;
        } catch (ClassNotFoundException e) {
            data = new String[1];
            data[0] = e.getMessage();
            return data;
        } catch (SQLException e) {
            data = new String[1];
            data[0] = e.getMessage();
            return data;
        } catch (Exception e) {
            data = new String[1];
            data[0] = e.getMessage();
            return data;
        }
    }

//TODO REVISAR QUE FUNCIONE CORRECTAMENTE

    public String buscarUsuario(String userName) throws Exception {
        Usuario u = dao.buscarUsuario(userName);
        if (u != null) {
            return u.toStringCSV();
        }
        return "";
    }

    public String eliminarUsuario(String userName ) throws Exception {
        Usuario u = dao.buscarUsuario(userName);
        if (u != null) {
            return dao.eliminarUsuario(userName);
        }
        return "";
    }
}
