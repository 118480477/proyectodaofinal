package Montero.Lizbeth.tl;

import Montero.Lizbeth.bl.categoria.Categoria;

import Montero.Lizbeth.bl.item.IItemDAO;
import Montero.Lizbeth.bl.item.Item;
import Montero.Lizbeth.bl.item.SqlServerItem;
import Montero.Lizbeth.bl.usuario.Usuario;
import Montero.Lizbeth.dao.DAOFactory;
import Montero.Lizbeth.utils.Utils;

import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;

public class ControllerItem {
    private IItemDAO dao;// encapsulamiento
    private DAOFactory factory;
    private ArrayList<Categoria> categorias;


    public ControllerItem() throws Exception {
        factory = DAOFactory.getDAOFactory(0);
        dao = factory.getItem();
    }

//TODO REVISAR QUE FUNCIONE CORRECTAMENTE

    public String buscarItem(String codigo) throws Exception {
        Item it = dao.buscarItem(codigo);
        if (it != null) {
            return it.toString();
        }
        return "no existe ";
    }

    public String modificarItem(String codigo, String nombre,String descripcion, String estado, String imagen, LocalDate fechaCompra, String fechaAntiguedad, String categoria_codigo ) throws Exception {
        Item it = new Item( codigo,  nombre, descripcion,  estado,  imagen,  fechaCompra,  fechaAntiguedad,  categoria_codigo);
        return  dao.modificarItem(it);
    }
    public String eliminarItem(String codigo) throws Exception {
        Item i = dao.buscarItem(codigo);
        if (i != null) {
            return dao.eliminarItem(codigo);
        }
        return "no existe";
    }


    public String registrarItem(String cd, String nom, String descr, String esta, String img, LocalDate fc, String ant, String ccat) throws Exception {
        Item i = new Item( cd,  nom, descr,  esta,  img,  fc,  ant, ccat);
        try {
            factory = DAOFactory.getDAOFactory(Utils.getMotorBD());// encapsulamiento
            dao = (IItemDAO) factory.getItem();

            return dao.insertar(i);
        } catch (SQLException e) {
            return e.getMessage();
        } catch (ClassNotFoundException e) {
            return e.getMessage();
        } catch (Exception e) {
            return e.getMessage();
        }
    }
    public ArrayList<String> listarObjeto() throws Exception {
        return   dao.listar();
    }


    public ArrayList<String> listarMisItems(String userNameColec) throws Exception {
        return   dao.listarMisItems(userNameColec);
    }

    public String asociarObjetosColeccionista(String usuario_userName, String codObjeto) throws Exception {
        return dao.asociarObjetosColeccionista(usuario_userName, codObjeto);
    }

}


