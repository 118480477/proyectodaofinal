package Montero.Lizbeth.bl.administrador;

import Montero.Lizbeth.bl.usuario.Usuario;

import java.time.LocalDate;

/**
 * Atributos de la clase administrador
 *
 * @author Lizbeth Montero
 * @version 1.1.0
 * @since 1.0.0
 */

public class Administrador extends Usuario {
    private String avatar, estado, correo,nombre;
    private String identificacion;
    private int edad;
    private LocalDate fechanacimiento;

    /**
     * contructor por defecto es utilizado para la invocación de las subclases, palabra reservada super que obtiene los metodos del padre en este caso del usuario
     */
    public Administrador(final String nombre, final String username, final String password, final String tipo) {
        super(nombre, username, password, tipo);
    }

    /**
     * Constructoe con todos los parametros de la clase Administrador
     * @param nombre          tipo String, nombre del administrador.
     * @param avatar          tipo String, identidad virtual del administrador.
     * @param estado          tipo String, activo o inactivo del administrador.
     * @param correo          tipo String,correo del administrador.
     * @param password        tipo String, contraseña del administrador.
     * @param identificacion  tipo int ,identificacion personal del administrador.
     * @param edad            tipo int, edad del administrador.
     * @param fechanacimiento tipo LocalDate, fecha especifica en la que nacio el administrador.
     */


    public Administrador(final String nombre, final String username, final String password, final String tipo, final String avatar, final String estado, final String correo, final String nombre1, final String identificacion, final int edad, final LocalDate fechanacimiento) {
        super(nombre, username, password, tipo);
        this.avatar = avatar;
        this.estado = estado;
        this.correo = correo;
        this.nombre = nombre1;
        this.identificacion = identificacion;
        this.edad = edad;
        this.fechanacimiento = fechanacimiento;
    }


    /**
     * constructor por defecto
     */
    public Administrador() {
        super();
    }
    /**
     * Constructoe con todos los parametros de la clase Administrador
     * @param nombre          tipo String, nombre del administrador.
     * @param avatar          tipo String, identidad virtual del administrador.
     * @param estado          tipo String, activo o inactivo del administrador.
     * @param correo          tipo String,correo del administrador.
     * @param password        tipo String, contraseña del administrador.
     * @param identificacion  tipo int ,identificacion personal del administrador.
     * @param edad            tipo int, edad del administrador.
     * @param fechanacimiento tipo LocalDate, fecha especifica en la que nacio el administrador.
     */
    public Administrador(String identificacion, String tipo, String nombre, String avatar, LocalDate fechanacimiento, int edad, String username, String correo, String password, String estado) {
        super(nombre, username, password, tipo);
        this.avatar = avatar;
        this.estado = estado;
        this.correo = correo;
        this.identificacion = identificacion;
        this.edad = edad;
        this.fechanacimiento = fechanacimiento;
    }


    /**
     * Metodo Getter de la variable que almacena el nombre, es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve el nombre del administrador, lo hace publico.
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param nombre: dato que almacena el nombre del administrador de manera privada en su calse respectiva.
     */

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * Metodo Getter de la variable que almacena el avatar, es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve el avatar del administrador, lo hace publico.
     */
    public String getAvatar() {
        return avatar;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param avatar: dato que almacena el avatar del administrador de manera privada en su calse respectiva.
     */
    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    /**
     * Metodo Getter de la variable que almacena el identificacion, es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve la identificacion del administrador, lo hace publico.
     */
    public String getIdentificacion() {
        return identificacion;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param identificacion: dato que almacena la identificacion del administrador de manera privada en su calse respectiva.
     */
    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    /**
     * Metodo Getter de la variable que almacena el estado, es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve el estado del administrador, lo hace publico.
     */
    public String getEstado() {
        return estado;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param estado: dato que almacena el estado del administrador de manera privada en su calse respectiva.
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }

    /**
     * Metodo Getter de la variable que almacena el correo, es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve el correo del administrador, lo hace publico.
     */
    public String getCorreo() {
        return correo;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param correo: dato que almacena el correo del administrador de manera privada en su calse respectiva.
     */
    public void setCorreo(String correo) {
        this.correo = correo;
    }

    /**
     * Metodo Getter de la variable que almacena la contrasennia, es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve la contrasennia del administrador, lo hace publico.
     */
    public String getPassword() {
        return password;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param password: dato que almacena la contrasennia del administrador de manera privada en su calse respectiva.
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Metodo Getter de la variable que almacena la edad, es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve la edad del administrador, lo hace publico.
     */
    public int getEdad() {
        return edad;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param edad: dato que almacena la edad del administrador de manera privada en su calse respectiva.
     */
    public void setEdad(int edad) {
        this.edad = edad;
    }

    /**
     * Metodo Getter de la variable que almacena la fecha de nacimiento, es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve la fecha de nacimiento del administrador, lo hace publico.
     */
    public LocalDate getFechanacimiento() {
        return fechanacimiento;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param fechanacimiento: dato que almacena la fecha de nacimiento del administrador de manera privada en su calse respectiva.
     */
    public void setFechanacimiento(LocalDate fechanacimiento) {
        this.fechanacimiento = fechanacimiento;
    }

    /**
     * Metodo toString utilizado para imprimir la in información almacenada en un String asignado.
     *
     * @return retorna los valores respectivos del administrador, en este caso el respectivo valor de cada atributo del administrador en un solo String.
     */



    @Override
    public String toString() {
        return "Administrador{" +
                "nombre='" + nombre +
                ", username='" + username +
                ", identificacion=" + identificacion +"\n" +
                ", correo='" + correo +
                ", password='" + password +
                ", avatar='" + avatar +
                ", estado='" + estado +
                ", edad=" + edad +
                ", fechanacimiento=" + fechanacimiento +
                ", password='" + password  +
                '}';
    }


    public void setIdentificacion(int edad) {
    }
}
