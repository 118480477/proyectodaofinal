package Montero.Lizbeth.bl.administrador;


import Montero.Lizbeth.Accesobd.Conector;
import Montero.Lizbeth.bl.categoria.Categoria;
import Montero.Lizbeth.bl.coleccionista.Coleccionista;
import Montero.Lizbeth.bl.item.Item;
import Montero.Lizbeth.utils.Utils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;

public class SqlServerAdministrador implements IAdministradorDAO {
    private String query;
    private ArrayList<String> administrador;

    /**
     * +
     * Metodo para insertar el administrador
     *
     * @param a tipo administrador que almacena el administrador que se va a registrar
     * @return mensaje, lista de administradores
     * @throws ClassNotFoundException exepcio que lanza una dvertencia que no encuentra los comandos que se le esta asignando.
     * @throws SQLException           exepcion tip sql laza una davertencia cuando hay un error en dicho motor
     * @throws Exception              exepciones general, padre de todas la exepciones
     */
    @Override
    public String insertar(Administrador a) throws ClassNotFoundException, SQLException, Exception {
        query = "INSERT INTO usuario (identificacion,tipo,nombre,userName,avatar,correo,contrasenia,estado,fechaNacimiento,edad) VALUES ('" +
                a.getIdentificacion() + "','" + a.getTipo() + "','" + a.getNombre() + "','" + a.getUsername() + "','" + a.getAvatar() + "','" + a.getCorreo() + "','" + a.getPassword() + "','" + a.getEstado() + "','" + a.getFechanacimiento() + "','" + a.getEdad() + "')";
        Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarQuery(query);
        return "El administrador se registró exitosamnete";
    }

    /**
     * metodo para listar los administradores
     *
     * @return un lista de administradores
     * @throws ClassNotFoundException exepcio que lanza una dvertencia que no encuentra los comandos que se le esta asignando.
     * @throws SQLException           exepcion tip sql laza una davertencia cuando hay un error en dicho motor
     * @throws Exception              exepciones general, padre de todas la exepciones
     */
    @Override
    public ArrayList<String> listar() throws ClassNotFoundException, SQLException, Exception {
        query = "SELECT identificacion,nombre,avatar,correo,estado,fechanacimiento,edad FROM usuario";
        administrador = new ArrayList<>();
        ResultSet rs = Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarSQL(query);
        while (rs.next()) {
            Administrador tmpAdministrador = new Administrador();
            tmpAdministrador.setIdentificacion(rs.getString("identificacion"));
            tmpAdministrador.setNombre(rs.getString("nombre"));
            tmpAdministrador.setNombre(rs.getString("avatar"));
            tmpAdministrador.setNombre(rs.getString("correo"));
            tmpAdministrador.setNombre(rs.getString("estado"));
            tmpAdministrador.setNombre(rs.getString("fechanacimiento"));
            tmpAdministrador.setIdentificacion(Integer.parseInt(rs.getString("edad")));
            administrador.add(tmpAdministrador.toString());
        }
        return administrador;
    }


    /**
     * metodo que busca al administrador
     * @param userName tipo String que almacena el userName que se va a buscar  que se va a registrar
     * @return un el userName del administrador
     *@throws Exception exepciones general, padre de todas la exepciones
     */

    public Administrador buscarAdministrador(String userName) throws Exception {
        query = "SELECT * FROM usuario WHERE userName ='" + userName + "'";
        ResultSet rs = Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarSQL(query);
        if (rs.next()) {
            Administrador A = new Administrador(
                    rs.getString("identificacion"),
                    rs.getString("tipo"),
                    rs.getString("nombre"),
                    rs.getString("avatar"),
                    LocalDate.parse(rs.getString("fechanacimiento")),
                    Integer.parseInt(rs.getString("edad")),
                    rs.getString("userName"),
                    rs.getString("correo"),
                    rs.getString("contrasenia"),
                    rs.getString("estado"));
            return A;
        }
        return null;
    }


}



