package Montero.Lizbeth.bl.administrador;

import java.sql.SQLException;
import java.util.ArrayList;

public interface IAdministradorDAO {
    /**
     * metodo de la interfaz que oblica a la clase sqlserverAdministradorDao a implementarlo
     * @param a tipo administrador que almacena el administrador que se va a registrar
     * @return un administrador
     * @throws ClassNotFoundException exepcio que lanza una dvertencia que no encuentra los comandos que se le esta asignando.
     * @throws SQLException exepcion tip sql laza una davertencia cuando hay un error en dicho motor
     * @throws Exception exepciones general, padre de todas la exepciones
     */
    String insertar(Administrador a) throws ClassNotFoundException, SQLException, Exception;

    /**
     * metodo de la interfaz que oblica a la clase sqlserverAdministradorDao a implementarlo
     * @return un lista de administradores
     @throws ClassNotFoundException exepcio que lanza una dvertencia que no encuentra los comandos que se le esta asignando.
      * @throws SQLException exepcion tip sql laza una davertencia cuando hay un error en dicho motor
     * @throws Exception exepciones general, padre de todas la exepciones
     */
    ArrayList<String> listar() throws ClassNotFoundException,SQLException,Exception;

    /**
     * metodo de la interfaz que oblica a la clase sqlserverAdministradorDao a implementarlo
     * @param userName tipo String que almacena el userName que se va a buscar  que se va a registrar
     * @return un el userName del administrador
     *@throws Exception exepciones general, padre de todas la exepciones
     */
    Administrador buscarAdministrador(String userName) throws Exception;
}
