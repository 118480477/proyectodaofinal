package Montero.Lizbeth.bl.item;

import Montero.Lizbeth.bl.categoria.Categoria;

import java.time.LocalDate;

/**
 * Atributos de la clase Item.
 *
 * @author Lizbeth Montero
 * @version 1.0.0
 * @since 1.0.0
 */
public class Item {

    private String nombre, descripcion, estado, imagen, categoria,id,categoria_codigo,codigo,antiguedad;
    private LocalDate fechaCompra;

    /**
     * contructor por defecto es utilizado para la invocación de las subclases.
     * @param codigo TIPO STRING VARIABLE QUE ALMACENA EL CODIGO DEL OBJETO
     * @param nombre TIPO STRING VARIABLE QUE ALMACENA EL NOMBRE DEL OBJETO
     * @param descripcion TIPO STRING VARIABLE QUE ALMACENA LA DESCRIPCION DEL OBJETO
     * @param estado TIPO STRING VARIABLE QUE ALMACENA  EL ESTADO DEL OBJETO
     * @param imagen TIPO STRING VARIABLE QUE ALMACENA LA IMAGEN  DEL OBJETO
     * @param fechaCompra TIPO LOCALDATE VARIABLE QUE ALMACENA LA FECHA DE COMPRA
     * @param antiguedad TIPO STRING  VARIABLE QUE ALMACENA LA ANTIGUEDAD
     * @param categoria_codigo TIPO STRING ALMACENA EL CODIGO DE LA CATEGORIA
     */

    public Item( String codigo, String nombre,String descripcion, String estado, String imagen, LocalDate fechaCompra, String antiguedad, String categoria_codigo) {

        this.nombre = nombre;
        this.descripcion = descripcion;
        this.estado = estado;
        this.imagen = imagen;
        this.categoria_codigo = categoria_codigo;
        this.codigo = codigo;
        this.fechaCompra = fechaCompra;
        this.antiguedad = antiguedad;
    }
    /**
     * contructor por defecto es utilizado para la invocación de las subclases.
     * @param codigo TIPO STRING VARIABLE QUE ALMACENA EL CODIGO DEL OBJETO
     * @param nombre TIPO STRING VARIABLE QUE ALMACENA EL NOMBRE DEL OBJETO
     * @param descripcion TIPO STRING VARIABLE QUE ALMACENA LA DESCRIPCION DEL OBJETO
     * @param estado TIPO STRING VARIABLE QUE ALMACENA  EL ESTADO DEL OBJETO
     * @param imagen TIPO STRING VARIABLE QUE ALMACENA LA IMAGEN  DEL OBJETO
     * @param fechaCompra TIPO LOCALDATE VARIABLE QUE ALMACENA LA FECHA DE COMPRA
     * @param antiguedad TIPO STRING  VARIABLE QUE ALMACENA LA ANTIGUEDAD
     * @param categoria_codigo TIPO STRING ALMACENA EL CODIGO DE LA CATEGORIA
     */
   public Item(String codigo, String nombre, String descripcion, String estado, String imagen, LocalDate fechaCompra, String antiguedad, String categoria_codigo, boolean add) {
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.estado = estado;
        this.imagen = imagen;
        this.categoria_codigo = categoria_codigo;
        this.codigo = codigo;
        this.fechaCompra = fechaCompra;
        this.antiguedad = antiguedad;
    }


    /**
     * Metodo Getter de la variable que almacena el nombre, es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve  el nombre del objeto, lo hace publico.
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param nombre: dato que almacena  el nombre del objeto de manera privada en su calse respectiva.
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * Metodo Getter de la variable que almacena la descripcion, es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve la descripcion del objeto, lo hace publico.
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param descripcion: dato que almacena  la descripcion del objeto de manera privada en su calse respectiva.
     */

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * Metodo Getter de la variable que almacena el estado, es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve  el estado, del objeto, lo hace publico.
     */
    public String getEstado() {
        return estado;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param estado: dato que almacena   el estado, del objeto de manera privada en su calse respectiva.
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }

    /**
     * Metodo Getter de la variable que almacena la imagen , es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve  la imagen, del objeto, lo hace publico.
     */
    public String getImagen() {
        return imagen;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param imagen: dato que almacena  la imagen, del objeto de manera privada en su calse respectiva.
     */
    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    /**
     * Metodo Getter de la variable que almacena la categoria , es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve  la categoria, del objeto, lo hace publico.
     */
    public String getCategoria() {
        return categoria;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param categoria: dato que almacena  la categoria, del objeto de manera privada en su calse respectiva.
     */
    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }


    /**
     * Metodo Getter de la variable que almacena la fechaCompra , es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve  la fechaCompra, del objeto, lo hace publico.
     */
    public LocalDate getFechaCompra() {
        return fechaCompra;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param fechaCompra: dato que almacena  la fechaCompra, del objeto de manera privada en su calse respectiva.
     */
    public void setFechaCompra(LocalDate fechaCompra) {
        this.fechaCompra = fechaCompra;
    }
    /**
     * Metodo Getter de la variable que almacena el codigo , es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve  la el id, del objeto, lo hace publico.
     */
    public String getId() {
        return id;
    }
    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param id: dato que almacena el ide de la subasta , del objeto de manera privada en su calse respectiva.
     */
    public void setId(String id) {
        this.id = id;
    }
    /**
     * Metodo Getter de la variable que almacena la el codigo de la categoria  , es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve  el codigo de la categoria , del objeto, lo hace publico.
     */
    public String getCategoria_codigo() {
        return categoria_codigo;
    }
    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param categoria_codigo: dato que almacena  el codigo de la categoria , del objeto de manera privada en su calse respectiva.
     */
    public void setCategoria_codigo(String categoria_codigo) {
        this.categoria_codigo = categoria_codigo;
    }
    /**
     * Metodo Getter de la variable que almacena el codigo de la subasta  , es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve  el codigo de la subasta , del objeto, lo hace publico.
     */
    public String getCodigo() {
        return codigo;
    }
    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param codigo: dato que almacena  el codigo de la subasta , del objeto de manera privada en su calse respectiva.
     */
    public String setCodigo(String codigo) {
        this.codigo = codigo;
        return codigo;
    }
    /**
     * Metodo Getter de la variable que almacena la fechaCompra , es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve  la fechaCompra, del objeto, lo hace publico.
     */
    public String getAntiguedad() {
        return this.antiguedad;
    }
    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param antiguedad: dato que almacena  la antiguedad del item , del objeto de manera privada en su calse respectiva.
     */
    public void setAntiguedad(final String antiguedad) {
        this.antiguedad = antiguedad;
    }
    /**
     * Metodo toString utilizado para imprimir la in información almacenada en un String asignado.
     * @return retorna los valores respectivos de los intereses, en este caso el respectivo valor de cada atributo de los intereses   en un solo String.
     */

    @Override
    public String toString() {
        return "Item{" +
                "nombre='" + nombre + '\'' +
                ", descripcion='" + descripcion + '\'' +
                ", estado='" + estado + '\'' +
                ", imagen='" + imagen + '\'' +
                ", categoria_codigo='" + categoria_codigo + '\'' +
                ", codigo='" + codigo + '\'' +
                ", antiguedad='" + antiguedad + '\'' +
                ", fechaCompra=" + fechaCompra +
                '}';
    }

    /**
     * Metodo toString utilizado para imprimir la in información almacenada en un String asignado.
     *
     * @return retorna los valores respectivos del objeto,con el respectivo valor de cada atributo en un solo String.
     */

    public String toStringCSV() {
        return nombre + "," + descripcion + "," + estado+","+imagen +","+categoria+","+id+","+fechaCompra;
    }

}//END CLASS
