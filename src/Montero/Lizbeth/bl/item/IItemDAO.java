package Montero.Lizbeth.bl.item;

import java.sql.SQLException;
import java.util.ArrayList;

public interface IItemDAO {

    /**
     * metodo de la interfaz que oblica a la clase sqlserverItemDao a implementarlo
     * @param i tipo item que almacena el interes  que se va a registrar
     * @return un mensaje, item
     * @throws ClassNotFoundException exepcio que lanza una dvertencia que no encuentra los comandos que se le esta asignando.
     * @throws SQLException exepcion tip sql laza una davertencia cuando hay un error en dicho motor
     * @throws Exception exepciones general, padre de todas la exepciones
     */
    String insertar(Item i) throws ClassNotFoundException, SQLException, Exception;
    /**
     * metodo de la interfaz que oblica a la clase sqlserverItemDao a implementarlo
     * @return  lista se items
     * @throws ClassNotFoundException exepcio que lanza una dvertencia que no encuentra los comandos que se le esta asignando.
     * @throws SQLException exepcion tip sql laza una davertencia cuando hay un error en dicho motor
     * @throws Exception exepciones general, padre de todas la exepciones
     */
    ArrayList<String> listar() throws ClassNotFoundException, SQLException, Exception;
    /**
     * metodo de la interfaz que oblica a la clase sqlserverItemDao a implementarlo
     * @param userName tipo string que almacena el item  que se va a listar
     * @return un mensaje, item
     * @throws ClassNotFoundException exepcio que lanza una dvertencia que no encuentra los comandos que se le esta asignando.
     * @throws SQLException exepcion tip sql laza una davertencia cuando hay un error en dicho motor
     * @throws Exception exepciones general, padre de todas la exepciones
     */
    ArrayList<String> listarMisItems(String userName) throws ClassNotFoundException, SQLException, Exception;
    /**
     * metodo de la interfaz que oblica a la clase sqlserverItemDao a implementarlo
     * @param codigo tipo string que almacena el item  que se va a eliminar
     * @return un mensaje, item
     * @throws ClassNotFoundException exepcio que lanza una dvertencia que no encuentra los comandos que se le esta asignando.
     * @throws SQLException exepcion tip sql laza una davertencia cuando hay un error en dicho motor
     * @throws Exception exepciones general, padre de todas la exepciones
     */
    String eliminarItem(String codigo) throws ClassNotFoundException, SQLException, Exception;
    /**
     * metodo de la interfaz que oblica a la clase sqlserverItemDao a implementarlo
     * @param items tipo item que almacena el item  que se va a modificar
     * @return un mensaje, item
     * @throws ClassNotFoundException exepcio que lanza una dvertencia que no encuentra los comandos que se le esta asignando.
     * @throws SQLException exepcion tip sql laza una davertencia cuando hay un error en dicho motor
     * @throws Exception exepciones general, padre de todas la exepciones
     */
    String modificarItem(Item items) throws ClassNotFoundException, SQLException, Exception;
    /**
     * metodo de la interfaz que oblica a la clase sqlserverItemDao a implementarlo
     * @param codigo tipo string que almacena el item  que se va a buscar
     * @return un mensaje, item
     * @throws ClassNotFoundException exepcio que lanza una dvertencia que no encuentra los comandos que se le esta asignando.
     * @throws SQLException exepcion tip sql laza una davertencia cuando hay un error en dicho motor
     * @throws Exception exepciones general, padre de todas la exepciones
     */
    Item buscarItem(String codigo) throws ClassNotFoundException, SQLException, Exception;
    /**
     * metodo de la interfaz que oblica a la clase sqlserverItemDao a implementarlo
     * @param codObjeto tipo String que almacena el item   que se va a asociar
     * @return un mensaje, item
     * @throws ClassNotFoundException exepcio que lanza una dvertencia que no encuentra los comandos que se le esta asignando.
     * @throws SQLException exepcion tip sql laza una davertencia cuando hay un error en dicho motor
     * @throws Exception exepciones general, padre de todas la exepciones
     */
    String asociarObjetosColeccionista(String userName, String codObjeto) throws Exception;
}
