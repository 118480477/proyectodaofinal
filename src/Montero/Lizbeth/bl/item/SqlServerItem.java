package Montero.Lizbeth.bl.item;

import Montero.Lizbeth.Accesobd.Conector;
import Montero.Lizbeth.bl.categoria.Categoria;
import Montero.Lizbeth.bl.coleccionista.Coleccionista;
import Montero.Lizbeth.bl.usuario.Usuario;
import Montero.Lizbeth.utils.Utils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;

public class SqlServerItem implements IItemDAO {
    private String query;
    private ArrayList<String> items;

    /**
     * metodo de insertar item
     * @param i tipo item que almacena el interes  que se va a registrar
     * @return un mensaje, item
     * @throws ClassNotFoundException exepcio que lanza una dvertencia que no encuentra los comandos que se le esta asignando.
     * @throws SQLException exepcion tip sql laza una davertencia cuando hay un error en dicho motor
     * @throws Exception exepciones general, padre de todas la exepciones
     */
    @Override
    public String insertar(Item i) throws ClassNotFoundException, SQLException, Exception {
        query = "INSERT INTO ITEM (codigo,nombre,descripcion,estado,imagen,fechaCompra,antiguedad,categoria_codigo) " +
                "VALUES" + " ('" + i.getCodigo() + "','" + i.getNombre() + "','" + i.getDescripcion() + "','" + i.getEstado() + "','"
                + i.getImagen() + "','"
                + i.getFechaCompra() + "','" + i.getAntiguedad() + "','" + i.getCategoria_codigo() + "')";
        Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarQuery(query);
        return "item  registrado exitosamente";
    }

    /**
     * metodo de listar item
     * @return  lista se items
     * @throws ClassNotFoundException exepcio que lanza una dvertencia que no encuentra los comandos que se le esta asignando.
     * @throws SQLException exepcion tip sql laza una davertencia cuando hay un error en dicho motor
     * @throws Exception exepciones general, padre de todas la exepciones
     */
    @Override
    public ArrayList<String> listar() throws ClassNotFoundException, SQLException, Exception {
    query = "SELECT * FROM ITEM";
    ArrayList<String> lista = new ArrayList<>();
    ResultSet rs = Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarSQL(query);
        while(rs.next()) {
        Item colec = new Item(
                rs.getString("codigo"),
                rs.getString("nombre"),
                rs.getString("descripcion"),
                rs.getString("estado"),
                rs.getString("imagen"),
                LocalDate.parse(rs.getString("fechaCompra")),
                rs.getString("antiguedad"),
                rs.getString("categoria_codigo"));
        lista.add(colec.toString());
    }
        return lista;

}
    /**
     * metodo de listar mis items
     * @param userName tipo string que almacena el item  que se va a listar
     * @return lista de , item
     * @throws ClassNotFoundException exepcio que lanza una dvertencia que no encuentra los comandos que se le esta asignando.
     * @throws SQLException exepcion tip sql laza una davertencia cuando hay un error en dicho motor
     * @throws Exception exepciones general, padre de todas la exepciones
     */

    @Override
    public ArrayList<String> listarMisItems(String userName) throws ClassNotFoundException, SQLException, Exception {
        query = "SELECT * FROM ITEM WHERE userName = ('" + userName + "')";

        ArrayList<String> lista = new ArrayList<>();
        ResultSet rs = Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarSQL(query);
        while(rs.next()) {
            Item colec = new Item(
                    rs.getString("codigo"),
                    rs.getString("nombre"),
                    rs.getString("descripcion"),
                    rs.getString("estado"),
                    rs.getString("imagen"),
                    LocalDate.parse(rs.getString("fechaCompra")),
                    rs.getString("antiguedad"),
                    rs.getString("categoria_codigo"));
            lista.add(colec.toString());
        }
        return lista;

    }
    /**
     * metodo de eliminar item
     * @param codigo tipo string que almacena el item  que se va a eliminar
     * @return un mensaje, item
     * @throws ClassNotFoundException exepcio que lanza una dvertencia que no encuentra los comandos que se le esta asignando.
     * @throws SQLException exepcion tip sql laza una davertencia cuando hay un error en dicho motor
     * @throws Exception exepciones general, padre de todas la exepciones
     */
    @Override
    public String eliminarItem(String codigo) throws ClassNotFoundException, SQLException, Exception {
        query = "DELETE FROM ITEM WHERE codigo= '" + codigo + "'";
        Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarSQL(query);
        return " Objeto eliminado de manera correcta";
    }
    /**
     * metodo de modificar item
     * @param items tipo item que almacena el item  que se va a modificar
     * @return un mensaje, item
     * @throws ClassNotFoundException exepcio que lanza una dvertencia que no encuentra los comandos que se le esta asignando.
     * @throws SQLException exepcion tip sql laza una davertencia cuando hay un error en dicho motor
     * @throws Exception exepciones general, padre de todas la exepciones
     */
    @Override
    public String modificarItem(Item items) throws ClassNotFoundException, SQLException, Exception {
        query = "UPDATE ITEM SET nombre='" + items.getNombre() + "'" +
                " WHERE codigo= '" + items.getId() + "'";
        Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarSQL(query);
        return "Item actualizado de manera correcta";
    }

    @Override
    /**
     * metodo de buscar item
     * @param codigo tipo string que almacena el item  que se va a buscar
     * @return un mensaje, item
     * @throws ClassNotFoundException exepcio que lanza una dvertencia que no encuentra los comandos que se le esta asignando.
     * @throws SQLException exepcion tip sql laza una davertencia cuando hay un error en dicho motor
     * @throws Exception exepciones general, padre de todas la exepciones
     */
    public Item buscarItem(String codigo) throws Exception {
        query = "SELECT * FROM ITEM WHERE codigo = '" + codigo + "'";
        ResultSet rs = Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarSQL(query);
        if (rs.next()) {
            Item item = new Item(
                    rs.getString("CODIGO"),
                    rs.getString("NOMBRE"),
                    rs.getString("DESCRIPCION"),
                    rs.getString("ESTADO"),
                    rs.getString("IMAGEN"),
                    LocalDate.parse(rs.getString("fechaCompra")),
                    rs.getString("antiguedad"),
                    rs.getString("categoria_codigo"));
            return item;
        }
        return null;
    }

    public Coleccionista buscarColeccionista(String userName) throws Exception {
        query = "SELECT * FROM usuario WHERE userName ='" + userName + "'";
        ResultSet rsColeccionista = Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarSQL(query);
        if (rsColeccionista.next()) {
            Coleccionista cole = new Coleccionista(rsColeccionista.getString("identificacion"),
                    rsColeccionista.getString("tipo"),rsColeccionista.getString("nombre"),
                    rsColeccionista.getString("avatar"),
                    LocalDate.parse(rsColeccionista.getString("fechanacimiento")),
                    Integer.parseInt(rsColeccionista.getString("edad")),
                    rsColeccionista.getString("userName"),
                    rsColeccionista.getString("correo"),
                    rsColeccionista.getString("contrasenia"),rsColeccionista.getString("estado"),
                    Double.parseDouble(rsColeccionista.getString("calificacion")),
                    rsColeccionista.getString("direccion"),Integer.parseInt(rsColeccionista.getString("esModerador")));
            return cole;
        }
        return null;
    }

    public Usuario buscarUsuario(String userName) throws ClassNotFoundException, SQLException, Exception {
        query = "SELECT * FROM usuario WHERE userName='" + userName + "'";
        ResultSet rsUsuario = Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarSQL(query);
        if (rsUsuario.next()) {
            Usuario u = new Usuario(rsUsuario.getString("nombre"), rsUsuario.getString("userName"), rsUsuario.getString("contrasenia"), rsUsuario.getString("tipo") );
            return u;
        }
        return null;
    }
    /**
     * metodo de asociar objeto
     * @param codObjeto tipo String que almacena el item   que se va a asociar
     * @return un mensaje, item
     * @throws ClassNotFoundException exepcio que lanza una dvertencia que no encuentra los comandos que se le esta asignando.
     * @throws SQLException exepcion tip sql laza una davertencia cuando hay un error en dicho motor
     * @throws Exception exepciones general, padre de todas la exepciones
     */
    @Override
    public String asociarObjetosColeccionista(String userName, String codObjeto) throws Exception {
        query = "INSERT INTO ITEM_usuario (usuario_userName, ITEM_codigo) VALUES ('" + userName + "','" + codObjeto + "')";
        Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarQuery(query);
        return "La item: " + codObjeto + " se agregó de forma correcta al coleccionista: " + userName;
    }


    public String listarMisItem (String userName) throws Exception {
        query = "SELECT * FROM ITEM WHERE userName = ('" + userName + ")";
        ArrayList<String> lista = new ArrayList<>();
        ResultSet rs = Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarSQL(query);
        if (rs.next()) {
            while(rs.next()) {
                Item colec = new Item(
                        rs.getString("codigo"),
                        rs.getString("nombre"),
                        rs.getString("descripcion"),
                        rs.getString("estado"),
                        rs.getString("imagen"),
                        LocalDate.parse(rs.getString("fechaCompra")),
                        rs.getString("antiguedad"),
                        rs.getString("categoria_codigo"));
                lista.add(colec.toString());
            }
        }
        return null;
    }
}
