package Montero.Lizbeth.bl.subasta;

import Montero.Lizbeth.bl.coleccionista.Coleccionista;
import Montero.Lizbeth.bl.item.Item;
import Montero.Lizbeth.bl.oferta.Oferta;

import java.time.LocalDate;
import java.util.ArrayList;


/**
 * Atributos de la clase Subasta y los atributos de sus respectivas relaciones.
 *
 * @author Lizbeth Montero
 * @version 1.0.0
 * @since 1.0.0
 */

public class Subasta {
    private String codigo, coleccionistaAA, userName_usuario;
    private LocalDate fechaCreacion, fechaInicio, FechaVencimiento;
    private int horaInicio;
    private int horaVencimiento;
    private int horaOferta;
    private Item objetosAVender;
    private double puntuacionPromedio, precioMinimoInicial;
    private String estadoSubasta, nombreVendedor, nombreDelColeccionista, ordenCompra_codigo;
    private ArrayList<Oferta> ofertas;
    private ArrayList<Item> items;

    /**
     *contructor con todos los parametros
     * @param codigo
     * @param fechaCreacion
     * @param fechaInicio tipo localDate variable que almace la fecha de inicio de la subasta
     * @param horaInicio tipo int almacena el hora de inicio de la subasta
     * @param FechaVencimiento tipo localDate variable que almace la fecha de vencimiento de la subasta
     * @param horaVencimiento tipo int almacena la hora de vencimiento de la subasta
     * @param estadoSubasta tipo string almacena el estado de la subasta
     * @param precioMinimoInicial tipo dooble variable que almacen el precio inicial de la subasta
     * @param ordenCompra_codigo tipo string almacena el codigo de la orden de compra de la usbasta
     * @param userName_usuario tipo string almacena el username del coleccionista que participoa en la subasta
     */
    public Subasta(String codigo, LocalDate fechaCreacion, LocalDate fechaInicio, int horaInicio, LocalDate FechaVencimiento, int horaVencimiento, String estadoSubasta, double precioMinimoInicial, String ordenCompra_codigo, String userName_usuario) {
        this.codigo = codigo;
        this.fechaCreacion = fechaCreacion;
        this.fechaInicio = fechaInicio;
        this.FechaVencimiento = FechaVencimiento;
        this.horaInicio = horaInicio;
        this.horaVencimiento = horaVencimiento;
        this.precioMinimoInicial = precioMinimoInicial;
        this.estadoSubasta = estadoSubasta;
        this.ordenCompra_codigo = "0";
        this.userName_usuario = userName_usuario;
        this.ofertas = new ArrayList<>();
        this.items = new ArrayList<>();

    }

    /**
     constructor que recibe un unico parametro
     * @param codigo tipo striing almacena el codigo de la subasta.
     */
    public Subasta(String codigo) {
        this.codigo = codigo;
        this.items = new ArrayList<>();
        this.ofertas = new ArrayList<>();
    }

    /**
     *Contructor con parametros
     * @param codigo strting amacena el codigo de la subasta
     * @param fechacreacion tipo localdate almacena la fecha de creacion de la subasta
     * @param fechainicio tipo localdate almacena la fecha de inicio de la subasta
     * @param horainicio tipo inte almacena  la hora de inicio de la subasta
     * @param fechavencimiento tipo localDate almacena la fecha de vencimiento de la subasta
     * @param horavencimiento tipo int almacena la hora de vencimiento de la subasta
     * @param estado tipo string almacena el estado de la subasta
     * @param preciobase tipo doble almacena el preio base de la subasta
     * @param esModerador tipo boolean almacena si el tipo de usuario es moderador o no.
     */
    public Subasta(String codigo, LocalDate fechacreacion, LocalDate fechainicio, int horainicio, LocalDate fechavencimiento, int horavencimiento, String estado, double preciobase, Coleccionista esModerador) {
    }

    /**
     * Metodo Getter de la variable que almacena el nombre de usuario del usuario de la subasta , es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve el username del coleccionista de la subasta, lo hace publico.
     */

    public String getUserName_usuario() {
        return this.userName_usuario;
    }
    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param userName_usuario : dato que almacena el userName_usuario de la subasta manera privada en su calse respectiva.
     */
    public void setUserName_usuario(final String userName_usuario) {
        this.userName_usuario = userName_usuario;
    }


    /**
     * Metodo Getter de la variable que almacena el codigo de la subasta , es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve el codigo de la subasta, lo hace publico.
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param codigo : dato que almacena el codigo de la subasta manera privada en su calse respectiva.
     */
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    /**
     * Metodo Getter de la variable que almacena la fecha de Creacion de la subasta , es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve la fecha de Creacion de la subasta, lo hace publico.
     */
    public LocalDate getFechaCreacion() {
        return fechaCreacion;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param fechaCreacion : dato que almacena la fecha de Creacion de la subasta manera privada en su calse respectiva.
     */
    public void setFechaCreacion(LocalDate fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    /**
     * Metodo Getter de la variable que almacena la fecha de Inicio de la subasta , es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve la fecha de Inicio de la subasta, lo hace publico.
     */
    public LocalDate getFechaInicio() {
        return fechaInicio;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param fechaInicio: dato que almacena la fecha de Inicio de la subasta manera privada en su calse respectiva.
     */
    public void setFechaInicio(LocalDate fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    /**
     * Metodo Getter de la variable que almacena la Fecha de Vencimientode la subasta , es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve la Fecha de Vencimiento de la subasta, lo hace publico.
     */
    public LocalDate getFechaVencimiento() {
        return FechaVencimiento;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param fechaVencimiento: dato que almacena la e fecha de Vencimiento de la subasta manera privada en su calse respectiva.
     */
    public void setFechaVencimiento(LocalDate fechaVencimiento) {
        FechaVencimiento = fechaVencimiento;
    }

    /**
     * Metodo Getter de la variable que almacena la hora de Inicio la subasta , es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve la horaInicio de la subasta, lo hace publico.
     */
    public int getHoraInicio() {
        return horaInicio;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param horaInicio: dato que almacena la hora de Inicio de la subasta manera privada en su calse respectiva.
     */
    public void setHoraInicio(int horaInicio) {
        this.horaInicio = horaInicio;
    }

    /**
     * Metodo Getter de la variable que almacena  los objetosAVender la subasta , es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve los objetosAVender de la subasta, lo hace publico.
     */
    public Item getObjetosAVender() {
        return objetosAVender;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param objetosAVender: dato que almacena los objetosAVender de la subasta manera privada en su calse respectiva.
     */
    public void setObjetosAVender(Item objetosAVender) {
        this.objetosAVender = objetosAVender;
    }

    /**
     * Metodo Getter de la variable que almacena  la puntuacionPromedio de la subasta , es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve la puntuacionPromedio de la subasta, lo hace publico.
     */
    public double getPuntuacionPromedio() {
        return puntuacionPromedio;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param puntuacionPromedio: dato que almacena la puntuacionPromedio de la subasta manera privada en su calse respectiva.
     */
    public void setPuntuacionPromedio(double puntuacionPromedio) {
        this.puntuacionPromedio = puntuacionPromedio;
    }

    /**
     * Metodo Getter de la variable que almacena el precioMinimoInicial de la subasta , es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve el precioMinimoInicial de la subasta, lo hace publico.
     */
    public double getPrecioMinimoInicial() {
        return precioMinimoInicial;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param precioMinimoInicial: dato que almacena el precioMinimoInicial de la subasta manera privada en su calse respectiva.
     */
    public void setPrecioMinimoInicial(double precioMinimoInicial) {
        this.precioMinimoInicial = precioMinimoInicial;
    }

    /**
     * Metodo Getter de la variable que almacena el estadoSubasta de la subasta , es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve el estadoSubasta de la subasta, lo hace publico.
     */
    public String getEstadoSubasta() {
        return estadoSubasta;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param estadoSubasta: dato que almacena el estadoSubasta de la subasta manera privada en su calse respectiva.
     */
    public void setEstadoSubasta(String estadoSubasta) {
        this.estadoSubasta = estadoSubasta;
    }

    /**
     * Metodo Getter de la variable que almacena el nombreVendedor de la subasta , es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve el nombreVendedor de la subasta, lo hace publico.
     */
    public String getNombreVendedor() {
        return nombreVendedor;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param nombreVendedor: dato que almacena el nombreVendedor de la subasta manera privada en su calse respectiva.
     */
    public void setNombreVendedor(String nombreVendedor) {
        this.nombreVendedor = nombreVendedor;
    }

    /**
     * Metodo Getter de la variable que almacena el nombreDelColeccionista de la subasta , es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve el nombreDelColeccionista de la subasta, lo hace publico.
     */
    public String getNombreDelColeccionista() {
        return nombreDelColeccionista;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param nombreDelColeccionista: dato que almacena el nombreDelColeccionista de la subasta manera privada en su calse respectiva.
     */
    public void setNombreDelColeccionista(String nombreDelColeccionista) {
        this.nombreDelColeccionista = nombreDelColeccionista;
    }

    /**
     * Metodo Getter de la variable que almacena las ofertas de la subasta , es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve las ofertas de la subasta, lo hace publico.
     */
    public ArrayList<Oferta> getOfertas() {
        return ofertas;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param ofertas: dato que almacena las ofertas de la subasta manera privada en su calse respectiva.
     */
    public void setOfertas(ArrayList<Oferta> ofertas) {
        this.ofertas = ofertas;
    }
    /**
     * Metodo Getter de la variable que almacena las ofertas de la subasta , es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve las ofertas de la subasta, lo hace publico.
     */
    public int getHoraVencimiento() {
        return horaVencimiento;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param horaVencimiento: dato que la hora de vencimiento las ofertas de la subasta manera privada en su calse respectiva.
     */
    public void setHoraVencimiento(int horaVencimiento) {
        this.horaVencimiento = horaVencimiento;
    }

    /**
     * Metodo Getter de la variable que almacena los objetos de la subasta , es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve las ofertas de la subasta, lo hace publico.
     */
    public ArrayList<Item> getItems() {
        return this.items;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param items: dato que almacena los items de la subasta manera privada en su calse respectiva.
     */
    public void setItems(final ArrayList<Item> items) {
        this.items = items;
    }

    /**
     * metodo utilizado para agregar ofertas
     * @param Codigo tipo string almacena el codigo de la oferta
     * @param PrecioOfertado tipo dooble almacena el precio ofertado a la subasta
     * @param FechaOferta tipo localdate almacena la fecha en que se relaizo la oferta
     * @param Coleccionistas tipo coleccionista almacena el coleccionita qie hace la oferta
     * @param SubastaCodigo tipo string almacena el codigo de la subasta
     */
    public void agregarOfertas(String Codigo, double PrecioOfertado, LocalDate FechaOferta, Coleccionista Coleccionistas, Subasta SubastaCodigo) {
        Oferta oferta = new Oferta(codigo, PrecioOfertado, FechaOferta, Coleccionistas, SubastaCodigo);
        ofertas.add(oferta);
    }
    /**
     * Metodo Getter de la variable que almacenael codigo de la orden de compra, es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve las el codigo de la orden de compra de la subasta, lo hace publico.
     */
    public String getOrdenCompra_codigo() {
        return ordenCompra_codigo;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param ordenCompra_codigo: dato que almacena el codigo de la orden de compra de la subasta manera privada en su calse respectiva.
     */
    public void setOrdenCompra_codigo(String ordenCompra_codigo) {
        this.ordenCompra_codigo = ordenCompra_codigo;
    }
    /**
     * Metodo Getter de la variable que almacena los coleccionistas  de la subasta , es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve los coleccionistas de la subasta, lo hace publico.
     */
    public String getColeccionistaAA() {
        return this.coleccionistaAA;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param coleccionistaAA: dato que almacena el coleccionista de la subasta manera privada en su calse respectiva.
     */
    public void setColeccionistaAA(final String coleccionistaAA) {
        this.coleccionistaAA = coleccionistaAA;
    }
    /**
     * Metodo Getter de la variable que almacena la hora en qe se hace la oferta de la subasta , es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve la hora  de la subasta, lo hace publico.
     */
    public int getHoraOferta() {
        return this.horaOferta;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param horaOferta: dato que almacena la horaOferta de la subasta manera privada en su calse respectiva.
     */
    public void setHoraOferta(final int horaOferta) {
        this.horaOferta = horaOferta;
    }
    /**
     * Metodo toString utilizado para imprimir la in información almacenada en un String asignado.
     *
     * @return retorna los valores respectivos de la subasta ,con el respectivo valor de cada atributo en un solo String.
     */

    @Override
    public String toString() {
        return "Subasta{" +
                "codigo='" + codigo + '\'' +
                ", userName_usuario='" + userName_usuario + '\'' +
                ", fechaCreacion=" + fechaCreacion +
                ", fechaInicio=" + fechaInicio +
                ", FechaVencimiento=" + FechaVencimiento +
                ", horaInicio=" + horaInicio +
                ", horaVencimiento=" + horaVencimiento +
                ", precioMinimoInicial=" + precioMinimoInicial +
                ", estadoSubasta='" + estadoSubasta + '\'' +
                '}';
    }

    /**
     * Método para imprimir la información de la subasta incluidas las ofertas realizadas
     *
     * @return texto, información de la subasta incluidas las ofertas realizadas
     */

    public String setEstadoSubasta() {
        return null;
    }

    public void add(Subasta tmpSubasta) {
    }

    public void setHoraInicio(String fechaInicio) {
    }

    public void agregarObjetos(Item item) {
    }

    /**
     * metodo to string de la oferta
     * @return devuelve la informacion de las ofertas
     */
    public String ofertaToString() {
        String infoOferta = "";
        for (Oferta oferta : ofertas) {
            infoOferta += oferta.toString() + "\n";
        }
        return infoOferta;
    }

    /**
     * metodo to string de objetos
     * @return devuelve la informacion de los objetos
     */
    public String objetoToString() {
        String infoObjetos = "";
        for (Item objeto : items) {
            infoObjetos += objeto.toString() + "\n";
        }
        return infoObjetos;
    }
}
