package Montero.Lizbeth.bl.subasta;

import Montero.Lizbeth.bl.coleccionista.Coleccionista;
import Montero.Lizbeth.bl.interes.Interes;

import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;

public interface ISubastaDAO {

    String insertarSubasta(Subasta s) throws ClassNotFoundException, SQLException, Exception;
    ArrayList<String> listarSubasta() throws ClassNotFoundException,SQLException,Exception;
    String eliminarSubasta(String codigo) throws ClassNotFoundException,SQLException,Exception;;
    Subasta buscarSubasta (String codigo) throws ClassNotFoundException,SQLException,Exception;;
  //  String modificarSubasta(Subasta subasta) throws ClassNotFoundException, SQLException, Exception ;
    String registrarSubasta(Subasta subasta, Coleccionista moderador) throws Exception;

    String modificarSubasta(Subasta subasta) throws ClassNotFoundException, SQLException, Exception;

    String modificarSubasta(Subasta subasta, Coleccionista moderador) throws Exception;

   // ArrayList<String> listarSubastasAbiertas() throws Exception;
    double listarMayorOferta(String codigo)throws Exception;
    ArrayList<String> listarObjetosSubasta(String codigo)throws Exception;
    ArrayList<String> listarOfertasSubasta(String codigo)throws Exception;
    int listarCantidadOferta(String codigo)throws Exception;
    String asociarObjetosSubasta(String idSubasta, String codObjeto)throws Exception;
    String asociarOfertaSubasta(String codigo, Double precioOfertado, LocalDate fechaOferta, String coleccionista, String subasta)throws Exception;
    Coleccionista buscarColeccionista(String userName) throws Exception;

    //String insertarSubasta(Subasta s) throws ClassNotFoundException, SQLException, Exception;
}
