package Montero.Lizbeth.bl.subasta;

import Montero.Lizbeth.Accesobd.Conector;
import Montero.Lizbeth.bl.categoria.Categoria;
import Montero.Lizbeth.bl.coleccionista.Coleccionista;
import Montero.Lizbeth.bl.item.Item;
import Montero.Lizbeth.bl.oferta.Oferta;
import Montero.Lizbeth.utils.Utils;
import com.microsoft.sqlserver.jdbc.SQLServerCallableStatement;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;


public class SqlServerSubasta implements ISubastaDAO {

    private String query;
    private ArrayList<String> subastas;

    public SqlServerSubasta() {
    }


    public String insertarSubasta(Subasta s) throws ClassNotFoundException, SQLException, Exception {
        query = "INSERT INTO subasta (codigo, fechaCreacion,fechaInicio,horaInicio,fechaVencimiento, " +
                "horaVencimiento, estado, precioBase, ordencompra_codigo,userName_Usuario) VALUES ('"
                + s.getCodigo() + "','" +
                s.getFechaCreacion() + "','" + s.getFechaInicio() + "'," + s.getHoraInicio() + ",'" +
                s.getFechaVencimiento() + "'," + s.getHoraVencimiento() + ",'" +
                s.getEstadoSubasta() + "'," + s.getPrecioMinimoInicial() + ",'" + s.getOrdenCompra_codigo() + "','" + s.getUserName_usuario() + "')";
        Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarQuery(query);
        return "Subasta registrada exitosamente";
    }

    public ArrayList<String> listarSubasta() throws ClassNotFoundException, SQLException, Exception {
        query = "SELECT codigo,fechaCreacion,fechaInicio,horaInicio,fechaVencimiento, horaVencimiento, estado, precioBase, ordencompra_codigo, userName_Usuario FROM subasta";
        subastas = new ArrayList<>();
        ResultSet rs = Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarSQL(query);
        while (rs.next()) {

            Subasta s = new Subasta(
                    rs.getString("codigo"),
                    LocalDate.parse(rs.getString("fechaCreacion")),
                    LocalDate.parse(rs.getString("fechaInicio")),
                    rs.getInt("horaInicio"),
                    LocalDate.parse(rs.getString("fechaVencimiento")),
                    rs.getInt("horaVencimiento"),
                    rs.getString("estado"),
                    rs.getDouble("precioBase"),
                    rs.getString("ordencompra_codigo"),
                    rs.getString("userName_Usuario"));
            subastas.add(s.toString());
        }
        return subastas;
    }


    @Override
    public String eliminarSubasta(String codigo) throws ClassNotFoundException, SQLException, Exception {
        query = "DELETE FROM subasta WHERE codigo= '" + codigo + "'";
        Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarSQL(query);
        return " Subasta eliminada de manera correcta";
    }


    @Override
    public Subasta buscarSubasta(String codigo) throws ClassNotFoundException, SQLException, Exception {
        query = "SELECT * FROM  subasta WHERE codigo ='" + codigo + "'";
        ResultSet rsSubasta = Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarSQL(query);
        if (rsSubasta.next()) {
            Subasta su = new Subasta(rsSubasta.getString("codigo"), LocalDate.parse(rsSubasta.getString("fechaCreacion")), LocalDate.parse(rsSubasta.getString("fechaInicio")),
                    rsSubasta.getInt("horaInicio"), LocalDate.parse(rsSubasta.getString("fechaVencimiento")), rsSubasta.getInt("horaVencimiento"),
                    rsSubasta.getString("estado"),
                    Double.parseDouble(rsSubasta.getString("precioBase")),
                    rsSubasta.getString("ordencompra_codigo"), rsSubasta.getString("userName_Usuario"));
            return su;
        }
        return null;
    }

    @Override
    public String registrarSubasta(Subasta subasta, Coleccionista moderador) throws Exception {
        return null;
    }


    public Item buscarItem(String codigo) throws Exception {
        query = "SELECT * FROM ITEM WHERE CODIGO = '" + codigo + "'";
        ResultSet rs = Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarSQL(query);
        if (rs.next()) {
            Item item = new Item(
                    rs.getString("CODIGO"),
                    rs.getString("NOMBRE"),
                    rs.getString("DESCRIPCION"),
                    rs.getString("ESTADO"),
                    rs.getString("IMAGEN"),
                    LocalDate.parse(rs.getString("fechaCompra")),
                    rs.getString("ANTIGUEDAD"),
                    rs.getString("categoria_codigo"));
            return item;
        }
        return null;
    }

    public Oferta buscarOferta(String codigo) throws Exception {
        query = "SELECT * FROM OFERTA WHERE codigo = '" + codigo + "'";
        ResultSet rs = Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarSQL(query);

        if (rs.next()) {
            Oferta oferta = new Oferta(
                    rs.getString("CODIGO"),
                    rs.getDouble("PRECIOOFERTADO"),
                    LocalDate.parse(rs.getString("FECHAOFERTA")),
                    new Coleccionista(rs.getString("userName_coleccionista")),
                    new Subasta(rs.getString("subasta_codigo")));
            return oferta;
        }

        return null;
    }


    @Override
    public String modificarSubasta(Subasta subasta) throws ClassNotFoundException, SQLException, Exception {
        query = "UPDATE subasta SET nombre='" + subasta.getEstadoSubasta() + "'" +
                " WHERE codigo= '" + subasta.getCodigo() + "'";
        Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarSQL(query);
        return "Interes actualizado de manera correcta";
    }


    @Override
    public String modificarSubasta(Subasta subasta, Coleccionista moderador) throws Exception {
        query = "UPDATE SUBASTA SET FECHAINICIO='" + subasta.getFechaInicio() +
                "',HORAINICIO=" + subasta.getHoraInicio() +
                ", FECHAVENCIMIENTO='" + subasta.getFechaVencimiento() +
                "',HORAVENCIMIENTO=" + subasta.getHoraVencimiento() +
                ", ESTADO='" + subasta.getEstadoSubasta() +
                "',PRECIOBASE=" + subasta.getPrecioMinimoInicial() +
                ", userName_Usuario='" + moderador.getUsername() +
                "' WHERE CODIGO='" + subasta.getCodigo() + "'";
        Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarSQL(query);
        return "Subasta actualizada de manera correcta";
    }

   public ArrayList<String> listarSubastasAbiertas() throws Exception {
        ArrayList<String> lista = new ArrayList<>();
        query = "SELECT * FROM SUBASTA WHERE ESTADO = 'ABIERTA'";
        ResultSet rs = Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarSQL(query);

        while (rs.next()) {
            Subasta subasta = new Subasta(
                    rs.getString("CODIGO"),
                    LocalDate.parse(rs.getString("FECHACREACION")),
                    LocalDate.parse(rs.getString("FECHAINICIO")),
                    rs.getInt("HORAINICIO"),
                    LocalDate.parse(rs.getString("FECHAVENCIMIENTO")),
                    rs.getInt("HORAVENCIMIENTO"),
                    rs.getString("ESTADO"),
                    rs.getDouble("PRECIOBASE"),
                    new Coleccionista(rs.getString("esModerador")));
            lista.add(subasta.toString());

        }
        return lista;
    }

    public double listarMayorOferta(String codigoSubasta) throws Exception {
        query = "SELECT MAX(PRECIOOFERTADO) as PRECIOMAX  FROM OFERTA WHERE subasta_codigo = '" +
                codigoSubasta + "' GROUP BY subasta_codigo";
        ResultSet rs = Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarSQL(query);

        while (rs.next()) {
            return rs.getDouble("PRECIOMAX");
        }
        return 0.0;
    }

    public int listarCantidadOferta(String codigoSubasta) throws Exception {

        query = "SELECT COUNT(*) AS OFERTAS FROM OFERTA WHERE subasta_codigo = '" +
                codigoSubasta + "' GROUP BY subasta_codigo";
        ResultSet rs = Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarSQL(query);

        while (rs.next()) {
            return rs.getInt("OFERTAS");
        }
        return 0;
    }

    //TODO
    public ArrayList<String> listarOfertasSubasta(String codigo) throws Exception {
        ArrayList<String> lista = new ArrayList<>();

        query = "SELECT * FROM SUBASTA WHERE CODIGO = '" + codigo + "'";
        ResultSet rsSubasta = Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarSQL(query);
        while (rsSubasta.next()) {
            Subasta subasta = buscarSubasta(rsSubasta.getString("CODIGO"));
            // Ahora hacemos un select de todos los objetos del colecionista
           String query1="";
            query1 = "SELECT C.CODIGO, CA.codo_oferta " +
                    "FROM OFERTASSUBASTA AS  CA, SUBASTA  AS C " +
                    "WHERE CA.cod_subasta='" + subasta.getCodigo() + "' " +
                    "AND CA.cod_subasta = C.CODIGO";
            ResultSet rsOfertas = Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarSQL(query1);

            while (rsOfertas.next()) {
                //Se crea el objeto para cada usuario
                Oferta oferta = buscarOferta(rsOfertas.getString("codo_oferta"));
                // Se agrega el objeto curso al arreglo de cursos de la carrera
                subasta.agregarOfertas(oferta.getCodigo(), oferta.getPrecioOfertado(), oferta.getFechaOferta(), oferta.getColeccinista(), oferta.getSubasta());
            }
                lista.add(subasta.ofertaToString());
            }
            return lista;
        }


        public ArrayList<String> listarObjetosSubasta (String codigo) throws Exception {
            ArrayList<String> lista = new ArrayList<>();

            query = "SELECT * FROM SUBASTA WHERE CODIGO = '" + codigo + "'";
            ResultSet rsSubasta = Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarSQL(query);

            while (rsSubasta.next()) {
                Subasta subasta = buscarSubasta(rsSubasta.getString("CODIGO"));
                // Ahora hacemos un select de todos los objetos del colecionista
                query = "SELECT C.CODIGO, CA.CODIGOOBJETO " +
                        "FROM OBJETOSSUBASTA CA, SUBASTA C " +
                        "WHERE CA.CODIGOSUBASTA='" + subasta.getCodigo() + "' " +
                        "AND CA.CODIGOSUBASTA = C.CODIGO";
                ResultSet rsItems = Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarSQL(query);
                while (rsItems.next()) {
                    //Se crea el objeto para cada usuario
                    Item item = buscarItem(rsItems.getString("CODIGOOBJETO"));
                    // Se agrega el objeto curso al arreglo de cursos de la carrera
                    subasta.agregarObjetos(item);
                }

                lista.add(subasta.objetoToString());
            }
            return lista;
        }

        public String asociarOfertaSubasta (String codigoOferta, Double precioOfertado, LocalDate fechaOferta, String
        coleccionista, String subasta) throws Exception {
            Coleccionista col = new Coleccionista(coleccionista);
            Subasta sub = new Subasta(subasta);
            Oferta oferta = new Oferta(codigoOferta, precioOfertado, fechaOferta, col, sub);
            query = "INSERT INTO OFERTASSUBASTA (cod_subasta, codo_oferta) VALUES ('" + subasta + "','" + codigoOferta + "')";
            Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarQuery(query);

            query = "INSERT INTO OFERTA (codigo, precioOfertado, fechaOferta, userName_coleccionista, subasta_codigo) VALUES('" +
                    oferta.getCodigo() + "'," +
                    oferta.getPrecioOfertado() + ",'" + oferta.getFechaOferta() + "','" +
                    col.getUsername() + "','" + sub.getCodigo() + "')";
            Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarQuery(query);

            return "La oferta: " + codigoOferta + " se agregó de forma correcta a la subasta: " + subasta;
        }
  /*  public String asociarOfertaSubasta(String codigoOferta, Double precioOfertado, LocalDate fechaOferta, int horaOferta, String coleccionista, String subasta) throws Exception {
        Coleccionista col = new Coleccionista(coleccionista);
        SQLServerCallableStatement rsSubasta = null;
        Subasta sub = new Subasta(rsSubasta.getString("codigo"),
                LocalDate.parse(rsSubasta.getString("fechaCreacion")),
                LocalDate.parse(rsSubasta.getString("fechaInicio")),
                rs.getInt("horaInicio"),
                LocalDate.parse(rsSubasta.getString("fechaVencimiento")),
                rs.getInt("horaVencimiento"),
                rsSubasta.getString("estado"),
                Double.parseDouble(rsSubasta.getString("precioBase")),
                rsSubasta.getString("ordencompra_codigo"), rsSubasta.getString("userName_Usuario"));
        Oferta oferta = new Oferta(codigoOferta, precioOfertado, fechaOferta, horaOferta, col, sub);
        query = "INSERT INTO OFERTASSUBASTA (cod_subasta, cod_oferta) VALUES ('" + subasta + "','" + codigoOferta + "')";
        Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarSQL(query);

        query = "INSERT INTO OFERTA (CODIGO, PRECIOOFERTADO, FechaOferta, userName_coleccionista,subasta_codigo ) VALUES('" +
                oferta.getCodigo() + "'," +
                oferta.getPrecioOfertado() + ",'" + oferta.getFechaOferta() + "'," +
                sub.getCodigo() + "','" + col.getIdentificacion() + "')";
        Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarSQL(query);

        return "La oferta: " + codigoOferta + " se agregó de forma correcta a la subasta: " + subasta;
    }
*/

        public String asociarObjetosSubasta (String codSubasta, String codObjeto) throws Exception {
            query = "INSERT INTO ITEM_subasta (subasta_codigo, ITEM_codigo) VALUES ('" + codSubasta + "','" + codObjeto + "')";
            Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarQuery(query);
            return "La item: " + codObjeto + " se agregó de forma correcta a la subasta: " + codSubasta;
        }


        @Override
        public Coleccionista buscarColeccionista (String userName) throws Exception {
            query = "SELECT * FROM USUARIO WHERE userName = '" + userName + "'";
            ResultSet rs = Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarSQL(query);

            if (rs.next()) {
                Coleccionista coleccionista = new Coleccionista(
                        rs.getString("identificacion"),
                        rs.getString("tipo"),
                        rs.getString("nombre"),
                        rs.getString("userName"),
                        rs.getString("estado"),
                        rs.getString("esModerador"),
                        rs.getDouble("calificacion"));
                return coleccionista;
            }
            return null;
        }


    }
