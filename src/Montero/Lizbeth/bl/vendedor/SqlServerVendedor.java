package Montero.Lizbeth.bl.vendedor;

import Montero.Lizbeth.Accesobd.Conector;

import Montero.Lizbeth.bl.coleccionista.Coleccionista;
import Montero.Lizbeth.bl.item.Item;
import Montero.Lizbeth.bl.subasta.Subasta;
import Montero.Lizbeth.utils.Utils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;

public class SqlServerVendedor implements IVendedorDAO {
    private String query;
    private ArrayList<String> vendedores;
    /**
     * metodo de insertar
     * @param v tipo vendedor que almacena el vendedor que se va a registrar
     * @return un mensaje, vendedor
     * @throws ClassNotFoundException exepcio que lanza una dvertencia que no encuentra los comandos que se le esta asignando.
     * @throws SQLException exepcion tip sql laza una davertencia cuando hay un error en dicho motor
     * @throws Exception exepciones general, padre de todas la exepciones
     */
    @Override
    public String insertarVendedor(Vendedor v) throws ClassNotFoundException, SQLException, Exception {
        query = "INSERT INTO vendedor (identificacion,nombre,apellido,correo,direccion,puntuacion) VALUES ('" + v.getIdentificacion() +
                "','" + v.getNombre() + "','" + v.getApellido() + "','" + v.getCorreo() + "','" + v.getDirecion() + "'," + v.getPuntuacion() + ")";
        Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarQuery(query);
        return "Vendedor  registrado exitosamente";
    }
    /**
     * metodo de listar
     * @return lista de vendedores
     * @throws ClassNotFoundException exepcio que lanza una dvertencia que no encuentra los comandos que se le esta asignando.
     * @throws SQLException exepcion tip sql laza una davertencia cuando hay un error en dicho motor
     * @throws Exception exepciones general, padre de todas la exepciones
     */
    @Override
    public ArrayList<String> listarVendedor() throws ClassNotFoundException, SQLException, Exception {
        query = "SELECT * FROM vendedor";
        vendedores = new ArrayList<>();
        ResultSet rs = Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarSQL(query);
        while (rs.next()) {
            Vendedor v = new Vendedor(
                    rs.getString("identificacion"),
                    rs.getString("nombre"),
                    rs.getString("apellido"),
                    rs.getString("correo"),
                    rs.getString("direccion"),
                    Double.parseDouble(rs.getString("puntuacion")));
            vendedores.add(v.toString());
        }
        return vendedores;
    }
    /**
     * metodo buscar item
     * @param codigo tipo string que almacena el coleccionista que se va a buscar
     * @return un mensaje, coleccionista
     * @throws ClassNotFoundException exepcio que lanza una dvertencia que no encuentra los comandos que se le esta asignando.
     * @throws SQLException exepcion tip sql laza una davertencia cuando hay un error en dicho motor
     * @throws Exception exepciones general, padre de todas la exepciones
     */

    public Item buscarItem(String codigo) throws Exception {
        query = "SELECT * FROM ITEM WHERE CODIGO = '" + codigo + "'";
        ResultSet rs = Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarSQL(query);
        if (rs.next()) {
            Item item = new Item(
                    rs.getString("CODIGO"),
                    rs.getString("NOMBRE"),
                    rs.getString("DESCRIPCION"),
                    rs.getString("ESTADO"),
                    rs.getString("IMAGEN"),
                    LocalDate.parse(rs.getString("fechaCompra")),
                    rs.getString("ANTIGUEDAD"),
                    rs.getString("categoria_codigo"));
            return item;
        }
        return null;
    }

    /**
     * metodo de eliminar
     * @param identificacion tipo string que almacena el coleccionista que se va a eliminar
     * @return un mensaje, vendedor
     * @throws ClassNotFoundException exepcio que lanza una dvertencia que no encuentra los comandos que se le esta asignando.
     * @throws SQLException exepcion tip sql laza una davertencia cuando hay un error en dicho motor
     * @throws Exception exepciones general, padre de todas la exepciones
     */
    @Override
    public String eliminarVendedor(String identificacion) throws ClassNotFoundException, SQLException, Exception {
        query = "DELETE FROM vendedor WHERE identificacion= '" + identificacion + "'";
        Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarSQL(query);
        return " vendedor eliminado de manera correcta";
    }
    /**
     * metodo buscar vendedor
     * @param identificacion tipo string que almacena el vendedor que se va a buscar
     * @return un mensaje, coleccionista
     * @throws ClassNotFoundException exepcio que lanza una dvertencia que no encuentra los comandos que se le esta asignando.
     * @throws SQLException exepcion tip sql laza una davertencia cuando hay un error en dicho motor
     * @throws Exception exepciones general, padre de todas la exepciones
     */
    @Override
    public Vendedor buscarVendedor(String identificacion) throws ClassNotFoundException, SQLException, Exception {
        query = "SELECT * FROM  vendedor WHERE identificacion ='" + identificacion + "'";
        ResultSet rsVendedor = Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarSQL(query);
        if (rsVendedor.next()) {
            Vendedor v = new Vendedor(rsVendedor.getString("identificacion"), rsVendedor.getString("nombre"), rsVendedor.getString("apellido"), rsVendedor.getString("correo"), rsVendedor.getString("direccion"), (rsVendedor.getDouble("puntuacion")));
            return v;
        }
        return null;
    }


    /**
     * metodo modificar vendedor
     * @param vendedor tipo coleccionista que almacena el vendedor que se va a modificar
     * @return un mensaje, coleccionista
     * @throws ClassNotFoundException exepcio que lanza una dvertencia que no encuentra los comandos que se le esta asignando.
     * @throws SQLException exepcion tip sql laza una davertencia cuando hay un error en dicho motor
     * @throws Exception exepciones general, padre de todas la exepciones
     */
    @Override
    public String modificarVendedro(Vendedor vendedor) throws ClassNotFoundException, SQLException, Exception {
        query = "UPDATE vendedor SET nombre='" + vendedor.getNombre() + "'," +
                "apellido'" + vendedor.getApellido() + "'," +
                "correo'" + vendedor.getCorreo() + "'," +
                "direccion'" + vendedor.getDirecion() + "'," +
                "puntuacio" + vendedor.getPuntuacion() + "" +
                " WHERE identificacion= '" + vendedor.getIdentificacion() + "'";
        Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarSQL(query);
        return "Vendedor actualizado de manera correcta";
    }

}