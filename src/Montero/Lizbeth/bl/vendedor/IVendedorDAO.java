package Montero.Lizbeth.bl.vendedor;

import Montero.Lizbeth.bl.categoria.Categoria;
import Montero.Lizbeth.bl.item.Item;

import java.sql.SQLException;
import java.util.ArrayList;

public interface IVendedorDAO {
    /**
     * metodo de la interfaz que oblica a la clase sqlserverVendedorDao a implementarlo
     * @param v tipo vendedor que almacena el vendedor que se va a registrar
     * @return un mensaje, vendedor
     * @throws ClassNotFoundException exepcio que lanza una dvertencia que no encuentra los comandos que se le esta asignando.
     * @throws SQLException exepcion tip sql laza una davertencia cuando hay un error en dicho motor
     * @throws Exception exepciones general, padre de todas la exepciones
     */
    String insertarVendedor(Vendedor v) throws ClassNotFoundException, SQLException, Exception;
    /**
     * metodo de la interfaz que oblica a la clase sqlserverVendedorDao a implementarlo
     * @return lista de vendedores
     * @throws ClassNotFoundException exepcio que lanza una dvertencia que no encuentra los comandos que se le esta asignando.
     * @throws SQLException exepcion tip sql laza una davertencia cuando hay un error en dicho motor
     * @throws Exception exepciones general, padre de todas la exepciones
     */
    ArrayList<String> listarVendedor() throws ClassNotFoundException,SQLException,Exception;
    /**
     * metodo de la interfaz que oblica a la clase sqlserverVendedorDao a implementarlo
     * @param identificacion tipo string que almacena el coleccionista que se va a eliminar
     * @return un mensaje, vendedor
     * @throws ClassNotFoundException exepcio que lanza una dvertencia que no encuentra los comandos que se le esta asignando.
     * @throws SQLException exepcion tip sql laza una davertencia cuando hay un error en dicho motor
     * @throws Exception exepciones general, padre de todas la exepciones
     */
    String  eliminarVendedor(String identificacion) throws ClassNotFoundException,SQLException,Exception;
    /**
     * metodo de la interfaz que oblica a la clase sqlserverVendedorDao a implementarlo
     * @param codigo tipo string que almacena el coleccionista que se va a buscar
     * @return un mensaje, coleccionista
     * @throws ClassNotFoundException exepcio que lanza una dvertencia que no encuentra los comandos que se le esta asignando.
     * @throws SQLException exepcion tip sql laza una davertencia cuando hay un error en dicho motor
     * @throws Exception exepciones general, padre de todas la exepciones
     */
    Item buscarItem (String codigo)throws ClassNotFoundException,SQLException,Exception;
    /**
     * metodo de la interfaz que oblica a la clase sqlserverVendedorDao a implementarlo
     * @param identificacion tipo string que almacena el vendedor que se va a buscar
     * @return un mensaje, coleccionista
     * @throws ClassNotFoundException exepcio que lanza una dvertencia que no encuentra los comandos que se le esta asignando.
     * @throws SQLException exepcion tip sql laza una davertencia cuando hay un error en dicho motor
     * @throws Exception exepciones general, padre de todas la exepciones
     */
    Vendedor buscarVendedor (String identificacion) throws ClassNotFoundException,SQLException,Exception;
    /**
     * metodo de la interfaz que oblica a la clase sqlserverVendedorDao a implementarlo
     * @param vendedor tipo coleccionista que almacena el vendedor que se va a modificar
     * @return un mensaje, coleccionista
     * @throws ClassNotFoundException exepcio que lanza una dvertencia que no encuentra los comandos que se le esta asignando.
     * @throws SQLException exepcion tip sql laza una davertencia cuando hay un error en dicho motor
     * @throws Exception exepciones general, padre de todas la exepciones
     */
    String modificarVendedro(Vendedor vendedor ) throws ClassNotFoundException, SQLException, Exception ;
}
