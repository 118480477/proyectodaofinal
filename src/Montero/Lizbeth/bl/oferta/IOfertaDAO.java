package Montero.Lizbeth.bl.oferta;

import Montero.Lizbeth.bl.coleccionista.Coleccionista;
import Montero.Lizbeth.bl.item.Item;
import Montero.Lizbeth.bl.subasta.Subasta;

import java.sql.SQLException;
import java.util.ArrayList;

public interface IOfertaDAO {

    /**
     * metodo de la interfaz que oblica a la clase sqlserverUsuarioDao a implementarlo
     * @param oferta tipo oferta que almacena el usuario que se va a registrar
     * @return un mensaje, usuario
     * @throws ClassNotFoundException exepcio que lanza una dvertencia que no encuentra los comandos que se le esta asignando.
     * @throws SQLException exepcion tip sql laza una davertencia cuando hay un error en dicho motor
     * @throws Exception exepciones general, padre de todas la exepciones
     */
        public String registrarOferta(Oferta oferta, Coleccionista coleccionista, Subasta subasta) throws Exception;
        public String modificarOferta(Oferta oferta, Coleccionista coleccionista, Subasta subasta) throws Exception;
        public String eliminarOferta(Oferta oferta) throws Exception;
        public Oferta buscarOferta(String codigo) throws Exception;
        public Coleccionista buscarColeccionista(String identificacion) throws Exception;
        public Subasta buscarSubasta(String codigoSubasta) throws Exception;
        public ArrayList<String> listarOfertas() throws Exception;
        public String asociarObjetoOferta(Oferta oferta, Item objeto) throws Exception;



    }
