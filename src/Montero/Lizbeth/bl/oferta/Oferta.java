package Montero.Lizbeth.bl.oferta;

import Montero.Lizbeth.bl.coleccionista.Coleccionista;
import Montero.Lizbeth.bl.item.Item;
import Montero.Lizbeth.bl.subasta.Subasta;

import java.time.LocalDate;
import java.util.ArrayList;

/**
 * Atributos de la clase Oferta y los atributos de sus respectivas relaciones.
 *
 * @author Lizbeth Montero
 * @version 2.0.0
 * @since 1.0.0
 */
public class Oferta {
    private int idOferente, horaInicio;
    private String codigo,subasta_codigo;
    private double puntuacionOferente, precioOfertado;
    private Item itemSubastado;
    private LocalDate fechaOferta;
    private String username; //revisar
    private Coleccionista coleccinista;
    private Subasta subasta;
    private ArrayList<Coleccionista> colecOfertando;

    /**
     * constructor con todos los parametros
     * @param codigo tipo string variable que almacena el codigo de la oferta
     * @param precioOfertado tipo dooble variable que almacena el variable quw almacena el precio ofertado
     * @param fechaOferta tipo Localdate variable que almacena el la fecha de orden de compra
     * @param coleccinista tipo coleccionista variable que almacena el coleccionista
     * @param subasta tipo subasta variable que almacena la subasta
     */
    public Oferta(String codigo, double precioOfertado, LocalDate fechaOferta, Coleccionista coleccinista, Subasta subasta) {
        this.codigo = codigo;
        this.precioOfertado = precioOfertado;
        this.fechaOferta = fechaOferta;
        this.coleccinista = coleccinista;
        this.subasta = subasta;
    }


    /**
     * Metodo Getter de la variable que almacena el idOferente , es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve el idOferente , de la oferta lo hace publico.
     */
    public int getIdOferente() {
        return idOferente;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param idOferente: dato que almacena  el idOferentea, de la oferta de manera privada en su calse respectiva.
     */
    public void setIdOferente(int idOferente) {
        this.idOferente = idOferente;
    }

    /**
     * Metodo Getter de la variable que almacena la puntuacionOferente , es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve la puntuacionOferente  , de la oferta lo hace publico.
     */
    public double getPuntuacionOferente() {
        return puntuacionOferente;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param puntuacionOferente : dato que almacena  la puntuacionOferente , de la oferta de manera privada en su calse respectiva.
     */
    public void setPuntuacionOferente(double puntuacionOferente) {
        this.puntuacionOferente = puntuacionOferente;
    }

    /**
     * Metodo Getter de la variable que almacena el precioOfertado , es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve el precioOfertado   , de la oferta lo hace publico.
     */
    public double getPrecioOfertado() {
        return precioOfertado;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param precioOfertado : dato que almacena el precioOfertado  , de la oferta de manera privada en su calse respectiva.
     */
    public void setPrecioOfertado(double precioOfertado) {
        this.precioOfertado = precioOfertado;
    }

    /**
     * Metodo Getter de la variable que almacena el itemSubastado , es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve el itemSubastado   , de la oferta lo hace publico.
     */
    public Item getItemSubastado() {
        return itemSubastado;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param itemSubastado : dato que almacena el itemSubastado  , de la oferta de manera privada en su calse respectiva.
     */
    public void setItemSubastado(Item itemSubastado) {
        this.itemSubastado = itemSubastado;
    }

    /**
     * Metodo Getter de la variable que almacena la fechaOferta , es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve la fechaOferta    , de la oferta lo hace publico.
     */
    public LocalDate getFechaOferta() {
        return fechaOferta;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param fechaOferta : dato que almacena la fechaOferta   , de la oferta de manera privada en su calse respectiva.
     */
    public void setFechaOferta(LocalDate fechaOferta) {
        this.fechaOferta = fechaOferta;
    }


    /**
     * Metodo Getter de la variable que almacena la subasta  , es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve la subasta   , de la oferta lo hace publico.
     */
    public Subasta getSubasta() {
        return this.subasta;
    }
    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param subasta : dato que almacena la subasta   , de la oferta de manera privada en su calse respectiva.
     */
    public void setSubasta(final Subasta subasta) {
        this.subasta = subasta;
    }

    /**
     * Metodo Getter de la variable que almacena la hora de inicio , es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve la hora de inicio    , de la oferta lo hace publico.
     */
    public int getHoraInicio() {
        return this.horaInicio;
    }
    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param horaInicio : dato que almacena la horaInicio   , de la oferta de manera privada en su calse respectiva.
     */
    public void setHoraInicio(final int horaInicio) {
        this.horaInicio = horaInicio;
    }

    /**
     * Metodo Getter de la variable que almacena el codigo de la subasta , es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve el codigo de ls subasta     , de la oferta lo hace publico.
     */
    public String getSubasta_codigo() {
        return this.subasta_codigo;
    }
    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param subasta_codigo : dato que almacena el subasta_codigo   , de la oferta de manera privada en su calse respectiva.
     */
    public void setSubasta_codigo(final String subasta_codigo) {
        this.subasta_codigo = subasta_codigo;
    }

    /**
     * Metodo Getter de la variable que almacena el user name  , es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve el user name    , de la oferta lo hace publico.
     */
    public String getUsername() {
        return this.username;
    }
    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param username : dato que almacena el username   , de la oferta de manera privada en su calse respectiva.
     */
    public void setUsername(final String username) {
        this.username = username;
    }

    public ArrayList<Coleccionista> getColecOfertando() {
        return colecOfertando;
    }
    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param colecOfertando : dato que almacena el coleccionista que esta ofertando, de la oferta de manera privada en su calse respectiva.
     */
    public void setColecOfertando(ArrayList<Coleccionista> colecOfertando) {
        this.colecOfertando = colecOfertando;
    }
    /**
     * Metodo Getter de la variable que almacena el codigo de la oferta , es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve el codigo de la oferta    , de la oferta lo hace publico.
     */
    public String getCodigo() {
        return codigo;
    }
    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param codigo : dato que almacena el codigo    , de la oferta de manera privada en su calse respectiva.
     */
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }
    /**
     * Metodo Getter de la variable que almacena el codigo de la subasta  , es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve el codigo de la subasta , de la oferta lo hace publico.
     */
    public String getSubastaCodigo() {
        return subasta_codigo;
    }
    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param subastaCodigo : dato que almacena el codigo de la subasta   , de la oferta de manera privada en su calse respectiva.
     */
    public void setSubastaCodigo(String subastaCodigo) {
        this.subasta_codigo = subastaCodigo;
    }

    /**
     * Metodo Getter de la variable que almacena el coleccionista , es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve el coleccionista   , de la oferta lo hace publico.
     */
    public Coleccionista getColeccinista() {
        return this.coleccinista;
    }
    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param coleccinista : dato que almacena el coleccionista   , de la oferta de manera privada en su calse respectiva.
     */
    public void setColeccinista(final Coleccionista coleccinista) {
        this.coleccinista = coleccinista;
    }
    /**
     * Metodo toString utilizado para imprimir la in información almacenada en un String asignado.
     *
     * @return retorna los valores respectivos de la oferta,con el respectivo valor de cada atributo en un solo String.
     */

    @Override
    public String toString() {
        return "Oferta{" +
                "codigo='" + codigo + '\'' +
                ", subasta_codigo='" + subasta_codigo + '\'' +
                ", precioOfertado=" + precioOfertado +
                ", fechaOferta=" + fechaOferta +
                '}';
    }

    /**
     * metodo que agrega la oferta.
     * @param tmpOferta
     */
    public void add(Oferta tmpOferta) {
    }


}
