package Montero.Lizbeth.bl.oferta;

//import Montero.Lizbeth.Accesobd.Conector;

import Montero.Lizbeth.Accesobd.Conector;
import Montero.Lizbeth.bl.coleccionista.Coleccionista;
import Montero.Lizbeth.bl.item.Item;
import Montero.Lizbeth.bl.subasta.Subasta;
import Montero.Lizbeth.utils.Utils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;

public class SqlServerOferta implements IOfertaDAO {

    private String query;
    private ArrayList<String> ofertas;


    public SqlServerOferta() {
    }

    /*public String insertar(Oferta o) throws ClassNotFoundException, SQLException, Exception {
        query = "INSERT INTO oferta (codigo,precioOfertado,fechaOferta,subasta_codigo) VALUES (" +
                "'" + o.getCodigo() + "','" + o.getPrecioOfertado() +
                "'," +o.getFechaOferta() + "','" + o.getSubastaCodigo()+ ")";
        //Conector.getConector(Montero.Lizbeth.utils.Utils.getProperties()[0],Montero.Lizbeth.utils.Utils.getProperties()[1]).ejecutarQuery(query);
        return "Orden de compra registrada exitosamente";
    }

    public ArrayList<String> listar() throws ClassNotFoundException, SQLException, Exception {
        query = "SELECT codigo,precioOfertado,fechaOferta,subasta_codigo FROM oferta";
        ArrayList<String> lista = new ArrayList<>();
        ResultSet rs = null;
        Conector.getConector(Montero.Lizbeth.utils.Utils.getProperties()[0], Montero.Lizbeth.utils.Utils.getProperties()[1]).ejecutarSQL(query);
        while (rs.next()) {

            Oferta of= new Oferta(
                    rs.getString("codigo"),
            Double.parseDouble(rs.getString("precioOfertado")),
            LocalDate.parse(rs.getString("fechaOferta")),
                    rs.getString("userNamen_coleccionista"),
                    rs.getString("subasta_codigo"));
            //TODO REVISAR
            //of.add(of);
            //of.add(of.toString());
        }
        return lista;
    }

    @Override
    public void eliminar(int codigo) {

    }*/
    public String registrarOferta(Oferta oferta, Coleccionista coleccionista, Subasta subasta) throws Exception {
        query = "INSERT INTO OFERTA (codigo, precioOfertado, fechaOferta, userName_coleccionista, subasta_codigo) VALUES('" +
                oferta.getCodigo() + "'," +
                oferta.getPrecioOfertado() + ",'" + oferta.getFechaOferta() + "','" +
                coleccionista.getUsername()  +"','" + subasta.getCodigo() +  "')";
        Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarQuery(query);
        return "Oferta registrada correctamente";
    }

    @Override
    public String modificarOferta(Oferta oferta, Coleccionista coleccionista, Subasta subasta) throws Exception {
        query = "UPDATE OFERTA SET PRECIOOFERTADO=" + oferta.getPrecioOfertado() + ", FECHAOFERTA='" + oferta.getFechaOferta() +
                "',subasta_codigo='" + subasta.getCodigo() + "', IDCOLECCIONISTA='" + coleccionista.getIdentificacion() +
                "' WHERE CODIGO='" + oferta.getCodigo() + "'";
        Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarQuery(query);
        return "Oferta actualizada de manera correcta";
    }

    @Override
    public String eliminarOferta(Oferta oferta) throws Exception {
        query = "DELETE FROM OFERTA WHERE CODIGO= '" + oferta.getCodigo() + "'";
        Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarQuery(query);
        return "Oferta eliminada de manera correcta";
    }

    @Override
    public Oferta buscarOferta(String codigo) throws Exception {
        query = "SELECT * FROM OFERTA WHERE CODIGO = '" + codigo + "'";
        ResultSet rs = Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarSQL(query);

        if (rs.next()) {
            Oferta oferta = new Oferta(
                    rs.getString("CODIGO"),
                    rs.getDouble("PRECIOOFERTADO"),
                    LocalDate.parse(rs.getString("FECHAOFERTA")),
                    new Coleccionista(rs.getString("userName_coleccionista")),
                    new Subasta(rs.getString("subasta_codigo")));
            return oferta;
        }

        return null;
    }

    @Override
    public Coleccionista buscarColeccionista(String userName) throws Exception {
        query = "SELECT * FROM usuario WHERE userName ='" + userName + "'";
        ResultSet rsColeccionista = Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarSQL(query);
        if (rsColeccionista.next()) {
            Coleccionista cole = new Coleccionista(rsColeccionista.getString("identificacion"),
                    rsColeccionista.getString("tipo"),rsColeccionista.getString("nombre"),
                    rsColeccionista.getString("avatar"),
                    LocalDate.parse(rsColeccionista.getString("fechanacimiento")),
                    Integer.parseInt(rsColeccionista.getString("edad")),
                    rsColeccionista.getString("userName"),
                    rsColeccionista.getString("correo"),
                    rsColeccionista.getString("contrasenia"),rsColeccionista.getString("estado"),
                    Double.parseDouble(rsColeccionista.getString("calificacion")),
                    rsColeccionista.getString("direccion"),Integer.parseInt(rsColeccionista.getString("esModerador")));
            return cole;
        }
        return null;
    }

    @Override
    public Subasta buscarSubasta(String codigo) throws ClassNotFoundException, SQLException, Exception {
        query = "SELECT * FROM  subasta WHERE codigo ='" + codigo + "'";
        ResultSet rsSubasta = Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarSQL(query);
        if (rsSubasta.next()) {
            Subasta su = new Subasta(rsSubasta.getString("codigo"), LocalDate.parse(rsSubasta.getString("fechaCreacion")), LocalDate.parse(rsSubasta.getString("fechaInicio")),
                    rsSubasta.getInt("horaInicio"), LocalDate.parse(rsSubasta.getString("fechaVencimiento")), rsSubasta.getInt("horaVencimiento"),
                    rsSubasta.getString("estado"),
                    Double.parseDouble(rsSubasta.getString("precioBase")),
                    rsSubasta.getString("ordencompra_codigo"), rsSubasta.getString("userName_Usuario"));
            return su;
        }
        return null;
    }

    @Override
    public ArrayList<String> listarOfertas() throws Exception {
        ArrayList<String> lista = new ArrayList<>();
        query = "SELECT * FROM OFERTA";
        ResultSet rs = Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarSQL(query);

        while (rs.next()) {
            Oferta oferta = new Oferta(
                    rs.getString("codigo"),
                    rs.getDouble("PRECIOOFERTADO"),
                    LocalDate.parse(rs.getString("FECHAOFERTA")),
                  new Coleccionista( rs.getString("userName_coleccionista")),
                   new Subasta(rs.getString("subasta_codigo"))) ;

            lista.add(oferta.toString());
        }
        return lista;
    }

    public ArrayList<String> listarColeccionistas() throws Exception {
        query =  "SELECT * FROM USUARIO";
        ArrayList<String> lista = new ArrayList<>();
        ResultSet rs = Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarSQL(query);
        while(rs.next()) {
            Coleccionista colec = new Coleccionista(
                    rs.getString("identificacion"),
                    rs.getString("TIPO"),
                    rs.getString("NOMBRE"),
                    rs.getString("AVATAR"),
                    LocalDate.parse(rs.getString("fechaNacimiento")),
                    rs.getInt("EDAD"),
                    rs.getString("userName"),
                    rs.getString("CORREO"),
                    rs.getString("CONTRASENIA"),
                    rs.getString("ESTADO"),
                    rs.getDouble("CALIFICACION"),
                    rs.getString("DIRECCION"),
                    rs.getInt("ESMODERADOR"));
            lista.add(colec.toString());
        }
        return lista;

    }

    @Override
    public String asociarObjetoOferta(Oferta oferta, Item objeto) throws Exception {

        return null;
    }


}


