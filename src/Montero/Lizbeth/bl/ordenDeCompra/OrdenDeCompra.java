package Montero.Lizbeth.bl.ordenDeCompra;

import Montero.Lizbeth.bl.coleccionista.Coleccionista;
import Montero.Lizbeth.bl.item.Item;
import Montero.Lizbeth.bl.subasta.Subasta;

import java.time.LocalDate;
import java.util.ArrayList;

/**
 * Atributos de la clase OrdenDeCompra y los atributos de sus respectivas relaciones.
 *
 * @author Lizbeth Montero
 * @version 1.0.0
 * @since 1.0.0
 */
public class OrdenDeCompra {
    private String codigo;
    private double precioTotal;
    private String detalleObjeto;
    private LocalDate fechaDeOrden;
    private Item itemComprado;
    private String id_comprador;
    private ArrayList<Item> items;
    private ArrayList<Coleccionista> compradorColect;

    /**
     * contructor por defecto es utilizado para la invocación de las subclases.
     */
    public OrdenDeCompra() {
    }

    /**
     * contructor con los atributos propios de la clase OrdenCompra.
     *
     * @param codigo        tipo int, codigo de la orden.
     * @param precioTotal   tipo double, precio total de la ordel.
     * @param detalleObjeto ripo String, detalle especifico del objeto.
     */
    public OrdenDeCompra(String codigo, double precioTotal, String detalleObjeto) {
        this.codigo = codigo;
        this.precioTotal = precioTotal;
        this.detalleObjeto = detalleObjeto;
    }

    /**
     * contructor con los atributos propios de la clase OrdenCompra y sus respectivas relaciones.
     *
     * @param codigo        tipo int, codigo de la orden.
     * @param precioTotal   tipo double, precio total de la ordel.
     * @param detalleObjeto tipo String, detalle especifico del objeto.
     * @param itemComprado  tipo Item, es el objeto en especifico que se compro.
     * @param fechaDeOrden  tipo LocalDate, fecha de cuando se realizo la orden.
     */
    public OrdenDeCompra(String codigo, double precioTotal, String detalleObjeto, LocalDate fechaDeOrden, Item itemComprado, ArrayList<Coleccionista> compradorColect) {
        this.codigo = codigo;
        this.precioTotal = precioTotal;
        this.detalleObjeto = detalleObjeto;
        this.fechaDeOrden = fechaDeOrden;
        this.itemComprado = itemComprado;
        this.compradorColect = compradorColect;
    }

  /*  public OrdenDeCompra(String codigo, LocalDate fecha, String precioTotal, String usuario_identififcacion) {
    }*/
//TODO REVISAR

    /**
     * Constructor
     *
     * @param codigo        tipo string  variable que almacen ael codigo de la orden de compra
     * @param fecha         tipo locadate almacena la fecah de orden de compra
     * @param horacompra    tipo int almacena la hora de compra
     * @param preciototal   tipo dooble amacena el precio total de la oferta
     * @param idusuario     tipo strig almacena el id del usuario
     * @param codigosubasta tipo string almacena el codigo de la subasta
     */
    public OrdenDeCompra(String codigo, LocalDate fecha, int horacompra, double preciototal, Coleccionista idusuario, Subasta codigosubasta) {
    }

    /**
     * @param codigo        tipo string  variable que almacen ael codigo de la orden de compra
     * @param fecha         tipo locadate almacena la fecah de orden de compra
     * @param horaCompra    tipo int almacena la hora de compra
     * @param precioTotal   tipo dooble amacena el precio total de la oferta
     * @param coleccionista tipo coleccionista almacena el coleccionista de la orden de compra
     * @param subasta
     */

    public OrdenDeCompra(String codigo, LocalDate fecha, int horaCompra, double precioTotal, Coleccionista coleccionista, String subasta) {
    }


    /**
     * Metodo Getter de la variable que almacena el codigo , es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve el codigo , de la orden de compra, lo hace publico.
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param codigo: dato que almacena  el codigo,  de la orden de compra de manera privada en su calse respectiva.
     */
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    /**
     * Metodo Getter de la variable que almacena el precio Total , es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve el precio Total , de la orden de compra, lo hace publico.
     */
    public double getPrecioTotal() {
        return precioTotal;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param precioTotal: dato que almacena  el precioTotal,  de la orden de compra de manera privada en su calse respectiva.
     */
    public void setPrecioTotal(double precioTotal) {
        this.precioTotal = precioTotal;
    }

    /**
     * Metodo Getter de la variable que almacena el detalle del Item , es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve el detalle del Item , de la orden de compra, lo hace publico.
     */
    public String getDetalleObjeto() {
        return detalleObjeto;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param detalleObjeto: dato que almacena  el detalle del Item,  de la orden de compra de manera privada en su calse respectiva.
     */
    public void setDetalleObjeto(String detalleObjeto) {
        this.detalleObjeto = detalleObjeto;
    }

    /**
     * Metodo Getter de la variable que almacena  la fecha De la Orden , es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve la fecha De la Orden , de la orden de compra, lo hace publico.
     */
    public LocalDate getFechaDeOrden() {
        return fechaDeOrden;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param fechaDeOrden: dato que almacena  la fecha De la Orden,  de la orden de compra de manera privada en su calse respectiva.
     */
    public void setFechaDeOrden(LocalDate fechaDeOrden) {
        this.fechaDeOrden = fechaDeOrden;
    }

    /**
     * Metodo Getter de la variable que almacena el itemComprado , es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve el itemComprado  , de la orden de compra, lo hace publico.
     */
    public Item getItemComprado() {
        return itemComprado;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param itemComprado: dato que almacena el itemComprado ,  de la orden de compra de manera privada en su calse respectiva.
     */
    public void setItemComprado(Item itemComprado) {
        this.itemComprado = itemComprado;
    }

   /* public void agregarColeccionista(String identificacion, String tipo, String nombre, String avatar,String username, LocalDate fechanacimiento, int edad, String correo, String contrasenia, String estado, double calificacion, String direccion, int esmoderador) {
        Coleccionista colecc = new Coleccionista( identificacion,  tipo,  nombre,  avatar, username,  fechanacimiento,  edad,  correo,  contrasenia,  estado,  calificacion,  direccion,  esmoderador);
        this.compradorColect.add(colecc);
    }*/

   /*public void agregarObjeto(String nombre, String descripcion, String estado, String imagen, String categoria,LocalDate  fechaAntiguedad, LocalDate fechaCompra, String id) {
        new Item(nombre, descripcion, estado, imagen, categoria, fechaAntiguedad, fechaCompra, id);
    }*/

    /**
     * Metodo Getter de la variable que almacena el id del comprador  , es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve el id del comprador , de la orden de compra, lo hace publico.
     */
    public String getId_comprador() {
        return id_comprador;
    }

    public void setId_comprador(String id_comprador) {
        this.id_comprador = id_comprador;
    }
    /**
     * Metodo Getter de la variable que almacena el id de la orden de compra, es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve el nombre del Ganador  , de la orden de compra, lo hace publico.
     */
    public ArrayList<Item> getItems() {
        return items;
    }

    public void setItems(ArrayList<Item> items) {
        this.items = items;
    }
    /**
     * Metodo Getter de la variable que almacena la lista de coleccionistas, es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve el nombre la lista de coleccionistas , de la orden de compra, lo hace publico.
     */
    public ArrayList<Coleccionista> getCompradorColect() {
        return compradorColect;
    }

    public void setCompradorColect(ArrayList<Coleccionista> compradorColect) {
        this.compradorColect = compradorColect;
    }

    /**
     * Metodo toString utilizado para imprimir la in información almacenada en un String asignado.
     *
     * @return retorna los valores respectivos de la orden de compra, en este caso el respectivo valor de cada atributo del administrador en un solo String.
     */
    @Override
    public String toString() {
        return "OrdenDeCompra{" +
                "codigo=" + codigo +
                ", precioTotal=" + precioTotal +
                ", detalleObjeto='" + detalleObjeto + '\'' +
                ", fechaDeOrden=" + fechaDeOrden +
                ", itemComprado=" + itemComprado +
                ", items=" + items +
                ", compradorColect=" + compradorColect +
                '}';
    }

    /**
     * metodo de identificacion de un objeto
     *
     * @param o tipo object, el objeto el cual se compara o se busca
     * @return true o false
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OrdenDeCompra that = (OrdenDeCompra) o;
        return getCodigo().equals(that.getCodigo());
    }

    public void add(OrdenDeCompra tmpOrden) {
    }
}
