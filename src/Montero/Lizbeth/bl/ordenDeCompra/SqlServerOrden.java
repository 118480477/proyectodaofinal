package Montero.Lizbeth.bl.ordenDeCompra;

import Montero.Lizbeth.Accesobd.Conector;
import Montero.Lizbeth.bl.coleccionista.Coleccionista;
import Montero.Lizbeth.bl.subasta.Subasta;
import Montero.Lizbeth.utils.Utils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;

public class SqlServerOrden implements IOrdenDeCompraDAO {

    private String query;
    private ArrayList<String> ordenes;


    public SqlServerOrden() {
    }


    @Override
    public String registrarOrdenCompra(OrdenDeCompra ordenCompra, Coleccionista coleccionista, Subasta subasta) throws Exception {

        query = "INSERT INTO ORDENCOMPRA(CODIGO, FECHA, PRECIOTOTAL, IDUSUARIO, CODIGOSUBASTA) VALUES('" + ordenCompra.getCodigo() + "','" +
                ordenCompra.getFechaDeOrden()  + "," + ordenCompra.getPrecioTotal() + ",'" + coleccionista.getUsername()
                + "','" + subasta.getCodigo() + "')";
        Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarSQL(query);
        return "Orden de compra registrada correctamente";
    }



    @Override
    public String modificarOrdenCompra(OrdenDeCompra ordenCompra, Coleccionista coleccionista, Subasta subasta) throws Exception {
        query = "UPDATE ORDENCOMPRA SET FECHA='" + ordenCompra.getFechaDeOrden() + "', PRECIOTOTAL=" + ordenCompra.getPrecioTotal() +
                ",IDUSUARIO='" + coleccionista.getIdentificacion() + "',CODIGO_SUBASATA='" + subasta.getCodigo() +
                "' WHERE CODIGO='" + ordenCompra.getCodigo() + "'";
        Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarSQL(query);
        return "Orden de compra actualizada de manera correcta";
    }

    @Override
    public String eliminarOrdenCompra(OrdenDeCompra ordenCompra) throws Exception {
        query = "DELETE FROM ORDENCOMPRA WHERE CODIGO= '" + ordenCompra.getCodigo() + "'";
        Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarSQL(query);
        return "Orden de compra eliminada de manera correcta";
    }

    @Override
    public OrdenDeCompra buscarOrdenCompra(String codigo) throws Exception {
        query = "SELECT * FROM ORDENCOMPRA WHERE CODIGO = '" + codigo + "'";
        ResultSet rs = Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarSQL(query);

        if (rs.next()) {
            OrdenDeCompra ordenCompra = new OrdenDeCompra(
                    rs.getString("CODIGO"),
                    LocalDate.parse(rs.getString("FECHA")),
                    rs.getInt("HORACOMPRA"),
                    rs.getDouble("PRECIOTOTAL"),
                    new Coleccionista(rs.getString("IDUSUARIO")),
                    new Subasta(rs.getString("CODIGOSUBASTA")));

            return ordenCompra;
        }

        return null;
    }

    @Override
    public Coleccionista buscarColeccionista(String identificacion) throws Exception {
        query = "SELECT * FROM USUARIO WHERE IDENTIFICACION = '" + identificacion + "'";
        ResultSet rs = Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarSQL(query);

        if (rs.next()) {
            Coleccionista coleccionista = new Coleccionista(
                    rs.getString("IDENTIFICACION"),
                    rs.getString("NOMBRE"),
                    rs.getString("CORREO"),
                    rs.getString("CONTRASENIA"),
                    rs.getString("ESTADO"),
                    rs.getString("DIRECCION"),
                    rs.getDouble("CALIFICACION"));
            return coleccionista;
        }
        return null;
    }
    @Override
    public Subasta buscarSubasta(String codigo) throws ClassNotFoundException, SQLException, Exception {
        query = "SELECT * FROM  subasta WHERE codigo ='" + codigo + "'";
        ResultSet rsSubasta = Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarSQL(query);
        if (rsSubasta.next()) {
            Subasta su = new Subasta(rsSubasta.getString("codigo"), LocalDate.parse(rsSubasta.getString("fechaCreacion")), LocalDate.parse(rsSubasta.getString("fechaInicio")),
                    rsSubasta.getInt("horaInicio"), LocalDate.parse(rsSubasta.getString("fechaVencimiento")), rsSubasta.getInt("horaVencimiento"),
                    rsSubasta.getString("estado"),
                    Double.parseDouble(rsSubasta.getString("precioBase")),
                    rsSubasta.getString("ordencompra_codigo"), rsSubasta.getString("userName_Usuario"));
            return su;
        }
        return null;
    }



    @Override
    public ArrayList<String> listarOrdenCompra() throws Exception {
        ArrayList<String> lista = new ArrayList<>();
        query = "SELECT * FROM ORDENCOMPRA";
        ResultSet rs = Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarSQL(query);

        while (rs.next()) {
            OrdenDeCompra ordenCompra = new OrdenDeCompra(
                    rs.getString("CODIGO"),
                    LocalDate.parse(rs.getString("FECHA")),
                    rs.getInt("HORA"),
                    rs.getDouble("PRECIOTOTAL"),
                    new Coleccionista(rs.getString("USUARIO_USERNAME")),
                    new Subasta(rs.getString("CODIGO_SUBASTA")));
            lista.add(ordenCompra.toString());
        }
        return lista;
    }


    @Override
    public String modificarColeccionistaOrdenCompra(OrdenDeCompra ordenCompra, Coleccionista coleccionista) throws Exception {
        query = "UPDATE ORDENCOMPRA SET IDUSUARIO = '" + coleccionista.getIdentificacion() + "' " +
                " WHERE CODIGO = '" + ordenCompra.getCodigo() + "')";
        Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarSQL(query);
        return "El coleccionista " + coleccionista.getIdentificacion() + " se  correctamente";
    }

    @Override
    public String modificarSubastaOrdenCompra(OrdenDeCompra ordenCompra, Subasta subasta) throws Exception {
        query = "UPDATE ORDENCOMPRA SET CODIGOSUBASTA = '" + subasta.getCodigo() + "' " +
                " WHERE CODIGO = '" + ordenCompra.getCodigo() + "')";
        Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarSQL(query);
        return "La subasta " + subasta.getCodigo() + " se modificÃ³ correctamente";
    }
}


