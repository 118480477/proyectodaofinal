package Montero.Lizbeth.bl.ordenDeCompra;

import Montero.Lizbeth.bl.coleccionista.Coleccionista;
import Montero.Lizbeth.bl.subasta.Subasta;

import java.util.ArrayList;

public interface IOrdenDeCompraDAO {


    public String registrarOrdenCompra(OrdenDeCompra ordenCompra, Coleccionista coleccionista, Subasta subasta) throws Exception;


    String modificarOrdenCompra(OrdenDeCompra ordenCompra, Coleccionista coleccionista, Subasta subasta) throws Exception;

    public String eliminarOrdenCompra(OrdenDeCompra ordenCompra) throws Exception;
    public OrdenDeCompra buscarOrdenCompra(String codigo) throws Exception;
    public Coleccionista buscarColeccionista(String identificacion) throws Exception;
    public Subasta buscarSubasta(String codigoSubasta) throws Exception;
    public ArrayList<String> listarOrdenCompra() throws Exception;
    public String modificarColeccionistaOrdenCompra(OrdenDeCompra ordenCompra, Coleccionista coleccionista) throws Exception;
   public   String modificarSubastaOrdenCompra(OrdenDeCompra ordenCompra, Subasta subasta) throws Exception;




}
