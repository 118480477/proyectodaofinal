package Montero.Lizbeth.bl.interes;

import Montero.Lizbeth.bl.categoria.Categoria;

import java.sql.SQLException;
import java.util.ArrayList;

public interface IInteresDAO {
    /**
     * metodo de la interfaz que oblica a la clase sqlserverInteresDao a implementarlo
     * @param in tipo interes que almacena el interes  que se va a registrar
     * @return un mensaje, interes
     * @throws ClassNotFoundException exepcio que lanza una dvertencia que no encuentra los comandos que se le esta asignando.
     * @throws SQLException exepcion tip sql laza una davertencia cuando hay un error en dicho motor
     * @throws Exception exepciones general, padre de todas la exepciones
     */
    String insertar(Interes in) throws ClassNotFoundException, SQLException, Exception;
    /**
     * metodo de la interfaz que oblica a la clase sqlserverInteresDao a implementarlo
     * @return lista de intereses
     * @throws ClassNotFoundException exepcio que lanza una dvertencia que no encuentra los comandos que se le esta asignando.
     * @throws SQLException exepcion tip sql laza una davertencia cuando hay un error en dicho motor
     * @throws Exception exepciones general, padre de todas la exepciones
     */
    ArrayList<String> listar() throws ClassNotFoundException,SQLException,Exception;
    /**
     * metodo de la interfaz que oblica a la clase sqlserverInteresDao a implementarlo
     * @param codigo tipo string que almacena el interes  que se va a eliminar
     * @return un mensaje, interes
     * @throws ClassNotFoundException exepcio que lanza una dvertencia que no encuentra los comandos que se le esta asignando.
     * @throws SQLException exepcion tip sql laza una davertencia cuando hay un error en dicho motor
     * @throws Exception exepciones general, padre de todas la exepciones
     */
    String eliminar(String codigo) throws ClassNotFoundException,SQLException,Exception;;
    /**
     * metodo de la interfaz que oblica a la clase sqlserverInteresDao a implementarlo
     * @param codigo tipo string que almacena el interes  que se va a buscar
     * @return un mensaje, interes
     * @throws ClassNotFoundException exepcio que lanza una dvertencia que no encuentra los comandos que se le esta asignando.
     * @throws SQLException exepcion tip sql laza una davertencia cuando hay un error en dicho motor
     * @throws Exception exepciones general, padre de todas la exepciones
     */
    Interes buscarInteres (String codigo) throws ClassNotFoundException,SQLException,Exception;;
    /**
     * metodo de la interfaz que oblica a la clase sqlserverInteresDao a implementarlo
     * @param interes tipo interes que almacena el interes  que se va a modificar
     * @return un mensaje, interes
     * @throws ClassNotFoundException exepcio que lanza una dvertencia que no encuentra los comandos que se le esta asignando.
     * @throws SQLException exepcion tip sql laza una davertencia cuando hay un error en dicho motor
     * @throws Exception exepciones general, padre de todas la exepciones
     */
    String modificarInteres(Interes interes) throws ClassNotFoundException, SQLException, Exception ;
}
