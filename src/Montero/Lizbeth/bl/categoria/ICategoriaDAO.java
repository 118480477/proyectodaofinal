package Montero.Lizbeth.bl.categoria;

import java.sql.SQLException;
import java.util.ArrayList;

public interface ICategoriaDAO {


    /**
     * metodo de la interfaz que oblica a la clase sqlserverCategoriaDao a implementarlo
     * @param c tipo categoria que almacena la categoria que se va a registrar
     * @return un categoria
     * @throws ClassNotFoundException exepcio que lanza una dvertencia que no encuentra los comandos que se le esta asignando.
     * @throws SQLException exepcion tip sql laza una davertencia cuando hay un error en dicho motor
     * @throws Exception exepciones general, padre de todas la exepciones
     */
    String insertar(Categoria c) throws ClassNotFoundException, SQLException, Exception;

    /**
     * metodo de la interfaz que oblica a la clase sqlserverCategoriaDao a implementarlo
     * @return una lista de  categoria
     * @throws ClassNotFoundException exepcio que lanza una dvertencia que no encuentra los comandos que se le esta asignando.
     * @throws SQLException exepcion tip sql laza una davertencia cuando hay un error en dicho motor
     * @throws Exception exepciones general, padre de todas la exepciones
     */
    ArrayList<String> listar() throws ClassNotFoundException,SQLException,Exception;


    /**
     * metodo de la interfaz que oblica a la clase sqlserverCategoriaDao a implementarlo
     * @param codigo tipo string que almacena el codigo de la categoria que se va a eliminar
     * @return un categoria
     * @throws ClassNotFoundException exepcio que lanza una dvertencia que no encuentra los comandos que se le esta asignando.
     * @throws SQLException exepcion tip sql laza una davertencia cuando hay un error en dicho motor
     * @throws Exception exepciones general, padre de todas la exepciones
     */
    String eliminar(String codigo)throws ClassNotFoundException,SQLException,Exception;

    /**
      * metodo de la interfaz que oblica a la clase sqlserverCategoriaDao a implementarlo
     * @param codigo tipo string varaiable que almacea el codigo de la categoria
     * @return retorna un mensaje
     @throws Exception exepciones general, padre de todas la exepciones
     */
    Categoria buscarCategoria (String codigo) throws Exception;


    /**
     * metodo de la interfaz que oblica a la clase sqlserverCategoriaDao a implementarlo
     * @param categoria tipo categoria que almacena la categoria que se va a modificar
     * @return un categoria
     * @throws ClassNotFoundException exepcio que lanza una dvertencia que no encuentra los comandos que se le esta asignando.
     * @throws SQLException exepcion tip sql laza una davertencia cuando hay un error en dicho motor
     * @throws Exception exepciones general, padre de todas la exepciones
     */
     String modificarCategoria(Categoria categoria) throws ClassNotFoundException, SQLException, Exception ;


    }
