package Montero.Lizbeth.bl.categoria;

import Montero.Lizbeth.Accesobd.Conector;
import Montero.Lizbeth.utils.Utils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class SqlServerCategoria implements ICategoriaDAO {
    private String query;
    private ArrayList<String> categorias;
    /**
     * metodo de insertar
     * @param c tipo categoria que almacena la categoria que se va a registrar
     * @return un mensaje
     * @throws ClassNotFoundException exepcio que lanza una dvertencia que no encuentra los comandos que se le esta asignando.
     * @throws SQLException exepcion tip sql laza una davertencia cuando hay un error en dicho motor
     * @throws Exception exepciones general, padre de todas la exepciones
     */
    @Override
    public String insertar(Categoria c) throws ClassNotFoundException, SQLException, Exception {
        query = "INSERT INTO categoria (codigo,nombre) VALUES ('" +
                c.getCodigo() + "','" + c.getNombre() + "')";
        Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarQuery(query);
        return "La categoria se registró exitosamnete";
    }

    /**
     * metodo de listar
     * @return una lista de  categoria
     * @throws ClassNotFoundException exepcio que lanza una dvertencia que no encuentra los comandos que se le esta asignando.
     * @throws SQLException exepcion tip sql laza una davertencia cuando hay un error en dicho motor
     * @throws Exception exepciones general, padre de todas la exepciones
     */
    @Override
    public ArrayList<String> listar() throws ClassNotFoundException, SQLException, Exception {
        query = "SELECT codigo, nombre FROM categoria";
        categorias = new ArrayList<>();
        ResultSet rs = Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarSQL(query);
        while (rs.next()) {
            Categoria tmpCategoria = new Categoria();
            tmpCategoria.setCodigo(rs.getString("codigo"));
            tmpCategoria.setNombre(rs.getString("nombre"));
            categorias.add(tmpCategoria.toStringCSV());
        }
        return categorias;
    }
    /**
     * metodo de modificar
     * @param categoria tipo categoria que almacena la categoria que se va a modificar
     * @return un mensaje,de categoria
     * @throws ClassNotFoundException exepcio que lanza una dvertencia que no encuentra los comandos que se le esta asignando.
     * @throws SQLException exepcion tip sql laza una davertencia cuando hay un error en dicho motor
     * @throws Exception exepciones general, padre de todas la exepciones
     */
    @Override
    public String modificarCategoria(Categoria categoria) throws
            ClassNotFoundException, SQLException, Exception {
        query = "UPDATE CATEGORIA SET nombre='" + categoria.getNombre() + "'" +
                " WHERE codigo= '" + categoria.getCodigo() + "'";
        Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarSQL(query);
        return "Categoría actualizada de manera correcta";
    }

    /**
     * metodo de eliminar
     * @param codigo tipo string varaiable que almacea el codigo de la categoria
     * @return retorna un mensaje
     @throws Exception exepciones general, padre de todas la exepciones
     */
    @Override
    public String eliminar(String codigo) throws ClassNotFoundException, SQLException, Exception {
        query = "DELETE FROM categoria WHERE codigo= '" + codigo + "'";
        Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarQuery(query);
        return "Categoría eliminada de manera correcta";
    }

    /**
     * metodo de la interfaz que oblica a la clase sqlserverCategoriaDao a implementarlo
     * @param codigo tipo string que almacena la categoria que se va a modificar
     * @return un categoria
     * @throws ClassNotFoundException exepcio que lanza una dvertencia que no encuentra los comandos que se le esta asignando.
     * @throws SQLException exepcion tip sql laza una davertencia cuando hay un error en dicho motor
     * @throws Exception exepciones general, padre de todas la exepciones
     */

    @Override
    public Categoria buscarCategoria(String codigo) throws Exception {

        query = "SELECT * FROM categoria WHERE codigo ='" + codigo + "'";
        ResultSet rsCategoria = Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarSQL(query);
        if (rsCategoria.next()) {
            Categoria categoria = new Categoria(rsCategoria.getString("codigo"), rsCategoria.getString("nombre"));
            return categoria;
        }
        return null;
    }
}

