package Montero.Lizbeth.bl.categoria;

import java.time.LocalDate;

/**
 * Atributos de la clase Categoria
 *
 * @author Lizbeth Montero
 * @version 1.1.0
 * @since 1.0.0
 */
public class Categoria {
    private String codigo, nombre;

    /**
     * Contructor que recibe un unico parametro.
     * @param codigo tipo string es el que almacena la categoria del objeto
     */
    public Categoria(String codigo) {
    }

    /**
     * Contructor con los parametros propios de la clase
     * @param codigo tiring, guarda el codigo de identificacion de la categoria
     * @param nombre ti po po StString guarda el nombre de la categoria.
     */
    public Categoria(String codigo, String nombre) {
        this.codigo = codigo;
        this.nombre = nombre;
    }

    /**
     * Contructor que recibe 2 parametros
     * @param codigo tiring, guarda el codigo de identificacion de la categoria
     * @param nombre ti po po int guarda el nombre de la categoria.
     */
    public Categoria(int codigo, String nombre) {
    }

    /**
     * contructor por defecto
     */
    public Categoria() {

    }



    /**
     * Metodo Getter de la variable que almacena el codigo, es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve el codigo de la categoria, lo hace publico.
     */
    public String getCodigo() {
        return codigo;
    }
    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param codigo: dato que almacena el codigo de la categoria de manera privada en su calse respectiva.
     */
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }
    /**
     * Metodo Getter de la variable que almacena el nombre, es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve el nombre de la categoria, lo hace publico.
     */
    public String getNombre() {
        return nombre;
    }
    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param nombre: dato que almacena el nombre de la categoria de manera privada en su calse respectiva.
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    /**
     * Metodo toString utilizado para imprimir la in información almacenada en un String asignado.
     *
     * @return retorna los valores respectivos de la categoria, en este caso el respectivo valor de cada atributo del administrador en un solo String.
     */
    @Override
    public String toString() {
        return "Categoria{" +
                "codigo='" + codigo + '\'' +
                ", nombre='" + nombre + '\'' +
                '}';
    }

        public String toStringCSV() {
        return codigo + ","+nombre ;
    }

}
