package Montero.Lizbeth.bl.usuario;

import Montero.Lizbeth.bl.categoria.Categoria;
import Montero.Lizbeth.bl.subasta.Subasta;

import java.sql.SQLException;
import java.util.ArrayList;

public interface IUsuarioDAO {
    /**
     * metodo de la interfaz que oblica a la clase sqlserverUsuarioDao a implementarlo
     * @param u tipo Uusuario que almacena el usuario que se va a registrar
     * @return un mensaje, usuario
     * @throws ClassNotFoundException exepcio que lanza una dvertencia que no encuentra los comandos que se le esta asignando.
     * @throws SQLException exepcion tip sql laza una davertencia cuando hay un error en dicho motor
     * @throws Exception exepciones general, padre de todas la exepciones
     */
    String insertarUsuario(Usuario u) throws ClassNotFoundException, SQLException, Exception;
    /**
     * metodo de la interfaz que oblica a la clase sqlserverUsuarioDao a implementarlo
     * @return lista de  usuario
     * @throws ClassNotFoundException exepcio que lanza una dvertencia que no encuentra los comandos que se le esta asignando.
     * @throws SQLException exepcion tip sql laza una davertencia cuando hay un error en dicho motor
     * @throws Exception exepciones general, padre de todas la exepciones
     */
    ArrayList<String> listarUsuario() throws ClassNotFoundException,SQLException,Exception;
    /**
     * metodo de la interfaz que oblica a la clase sqlserverUsuarioDao a implementarlo
     * @param userName tipo String que almacena el usuario que se va a eliminar
     * @return un mensaje, usuario
     * @throws ClassNotFoundException exepcio que lanza una dvertencia que no encuentra los comandos que se le esta asignando.
     * @throws SQLException exepcion tip sql laza una davertencia cuando hay un error en dicho motor
     * @throws Exception exepciones general, padre de todas la exepciones
     */
    String eliminarUsuario(String userName)throws ClassNotFoundException,SQLException,Exception;;
    /**
     * metodo de la interfaz que oblica a la clase sqlserverUsuarioDao a implementarlo
     * @param pasword tipo String que almacena la contrasenia  que se va a eliminar
     * @return un mensaje, usuario
     * @throws ClassNotFoundException exepcio que lanza una dvertencia que no encuentra los comandos que se le esta asignando.
     * @throws SQLException exepcion tip sql laza una davertencia cuando hay un error en dicho motor
     * @throws Exception exepciones general, padre de todas la exepciones
     */
    String eliminarpasword(String pasword)throws ClassNotFoundException,SQLException,Exception;;
    /**
     * metodo de la interfaz que oblica a la clase sqlserverUsuarioDao a implementarlo
     * @param userName tipo string que almacena el usuario que se va a buscar
     * @return  usuario
     * @throws ClassNotFoundException exepcio que lanza una dvertencia que no encuentra los comandos que se le esta asignando.
     * @throws SQLException exepcion tip sql laza una davertencia cuando hay un error en dicho motor
     * @throws Exception exepciones general, padre de todas la exepciones
     */
    Usuario buscarUsuario (String userName) throws ClassNotFoundException,SQLException,Exception;;
    /**
     * metodo de la interfaz que oblica a la clase sqlserverUsuarioDao a implementarlo
     * @param u tipo Uusuario que almacena el usuario que se va a modificar
     * @return  usuario
     * @throws ClassNotFoundException exepcio que lanza una dvertencia que no encuentra los comandos que se le esta asignando.
     * @throws SQLException exepcion tip sql laza una davertencia cuando hay un error en dicho motor
     * @throws Exception exepciones general, padre de todas la exepciones
     */
    String modificarPasword(Usuario u) throws ClassNotFoundException, SQLException, Exception ;


}
