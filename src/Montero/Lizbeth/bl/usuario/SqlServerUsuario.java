package Montero.Lizbeth.bl.usuario;

import Montero.Lizbeth.Accesobd.Conector;
import Montero.Lizbeth.utils.Utils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class SqlServerUsuario implements IUsuarioDAO {
    private String query;
    private ArrayList<String> usuarios;

    /**
     * metodo insertar
     * @param u tipo Uusuario que almacena el usuario que se va a registrar
     * @return un mensaje, usuario
     * @throws ClassNotFoundException exepcio que lanza una dvertencia que no encuentra los comandos que se le esta asignando.
     * @throws SQLException exepcion tip sql laza una davertencia cuando hay un error en dicho motor
     * @throws Exception exepciones general, padre de todas la exepciones
     */
    @Override
    public String insertarUsuario(Usuario u) throws ClassNotFoundException, SQLException, Exception {
        query = "INSERT INTO usuario (nombre, userName ,contrasenia,tipo ) VALUES ('" + u.getNombre() + "','" + u.getUsername() + "','" + u.getPassword() +"','"+u.getTipo()+"')";
        Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarQuery(query);
        return "El usuario se registró exitosamnete";
    }

    /**
     * metodo listar
     * @return lista de  usuario
     * @throws ClassNotFoundException exepcio que lanza una dvertencia que no encuentra los comandos que se le esta asignando.
     * @throws SQLException exepcion tip sql laza una davertencia cuando hay un error en dicho motor
     * @throws Exception exepciones general, padre de todas la exepciones
     */
    @Override
    public ArrayList<String> listarUsuario() throws ClassNotFoundException, SQLException, Exception {
        query = "SELECT  nombre, userName, contrasenia, tipo FROM usuario";
        usuarios = new ArrayList<>();
        ResultSet rs = Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarSQL(query);
        while (rs.next()) {
            Usuario u = new Usuario();
            u.setNombre(rs.getString("nombre"));
            u.setUsername(rs.getString("userName"));
            u.setPassword(rs.getString("contrasenia"));
            u.setTipo(rs.getString("tipo"));
            usuarios.add(u.toStringCSV());
        }
        return usuarios;
    }
    /**
     * metodo eliminar
     * @param userName tipo String que almacena el usuario que se va a eliminar
     * @return un mensaje, usuario
     * @throws ClassNotFoundException exepcio que lanza una dvertencia que no encuentra los comandos que se le esta asignando.
     * @throws SQLException exepcion tip sql laza una davertencia cuando hay un error en dicho motor
     * @throws Exception exepciones general, padre de todas la exepciones
     */
    @Override
    public String eliminarUsuario(String userName) throws ClassNotFoundException, SQLException, Exception {
        query = "DELETE FROM usuario WHERE userName= '" + userName + "'";
        Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarQuery(query);
        return " Usuario eliminado de manera correcta";
    }
    /**
     * metodo eliminar contrasenia
     * @param pasword tipo String que almacena la contrasenia  que se va a eliminar
     * @return un mensaje, usuario
     * @throws ClassNotFoundException exepcio que lanza una dvertencia que no encuentra los comandos que se le esta asignando.
     * @throws SQLException exepcion tip sql laza una davertencia cuando hay un error en dicho motor
     * @throws Exception exepciones general, padre de todas la exepciones
     */
    @Override
    public String eliminarpasword(String pasword) throws ClassNotFoundException, SQLException, Exception {
        return null;
    }
    /**
     * metodo buscra usuario
     * @param userName tipo string que almacena el usuario que se va a buscar
     * @return  usuario
     * @throws ClassNotFoundException exepcio que lanza una dvertencia que no encuentra los comandos que se le esta asignando.
     * @throws SQLException exepcion tip sql laza una davertencia cuando hay un error en dicho motor
     * @throws Exception exepciones general, padre de todas la exepciones
     */
    @Override
    public Usuario buscarUsuario(String userName) throws ClassNotFoundException, SQLException, Exception {
        query = "SELECT * FROM usuario WHERE userName='" + userName + "'";
        ResultSet rsUsuario = Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarSQL(query);
        if (rsUsuario.next()) {
            Usuario u = new Usuario(rsUsuario.getString("nombre"), rsUsuario.getString("userName"), rsUsuario.getString("contrasenia"), rsUsuario.getString("tipo") );
            return u;
        }
        return null;
    }
    /**
     * metodo modificar contrasenia
     * @param u tipo Uusuario que almacena el usuario que se va a modificar
     * @return  usuario
     * @throws ClassNotFoundException exepcio que lanza una dvertencia que no encuentra los comandos que se le esta asignando.
     * @throws SQLException exepcion tip sql laza una davertencia cuando hay un error en dicho motor
     * @throws Exception exepciones general, padre de todas la exepciones
     */
    @Override
    public String modificarPasword(Usuario u) throws ClassNotFoundException, SQLException, Exception {
        return null;
    }
}

