package Montero.Lizbeth.bl.coleccionista;


import Montero.Lizbeth.Accesobd.Conector;

import Montero.Lizbeth.bl.item.Item;
import Montero.Lizbeth.bl.subasta.Subasta;
import Montero.Lizbeth.utils.Utils;

import java.io.FileInputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;

public class SqlServerColeccionista implements IColeccionistaDAO {
    private String query;
    private ArrayList<String> coleccionistas;
    private FileInputStream fis;

    public SqlServerColeccionista() {
    }
    /**
     * metodo de ingresar coleccionista
     * @param col tipo coleccionista que almacena el coleccionista que se va a registrar
     * @return un mensaje, coleccionista
     * @throws ClassNotFoundException exepcio que lanza una dvertencia que no encuentra los comandos que se le esta asignando.
     * @throws SQLException exepcion tip sql laza una davertencia cuando hay un error en dicho motor
     * @throws Exception exepciones general, padre de todas la exepciones
     */
    @Override
    public String registrarColeccionista(Coleccionista col) throws Exception {
        query = "INSERT INTO usuario (identificacion,tipo,nombre,avatar,userName,fechanacimiento,edad,correo,contrasenia" +
                ",estado,calificacion,direccion,esModerador) VALUES ('" +
                col.getIdentificacion() + "','" + col.getTipo() + "','" + col.getNombre() + "','" + col.getAvatar() + "','" + col.getUsername()
                + "','" + col.getFechaNacimiento() + "',"
                + col.getEdad() + ",'" + col.getCorreo() + "','" + col.getPassword() + "','" + col.getEstado() + "'," + col.getCalificacion() +
                ",'" + col.getDireccion() + "',"
                + col.getModerador() + ")";
        Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarQuery(query);
        return "El coleccionista se registró exitosamnete";
    }


    /**
     * metodo de modificar coleccionista
     * @param coleccionista tipo coleccionista que almacena el coleccionista que se va a modificar
     * @return un coleccinista, mensaje
     * @throws ClassNotFoundException exepcio que lanza una dvertencia que no encuentra los comandos que se le esta asignando.
     * @throws SQLException exepcion tip sql laza una davertencia cuando hay un error en dicho motor
     * @throws Exception exepciones general, padre de todas la exepciones
     */
    @Override
    public String modificarColeccionista(Coleccionista coleccionista) throws Exception {
        query = "UPDATE usuario SET identificacion='" + coleccionista.getIdentificacion() + "'," +
                "tipo='" + coleccionista.getTipo() + "'," +
                "nombre='" + coleccionista.getNombre() + "'," +
                "avatar='" + coleccionista.getAvatar() + "'," +
                "fechaNacimiento='" + coleccionista.getFechaNacimiento() + "'," +
                "edad=" + coleccionista.getEdad() + "," +
                "correo='" + coleccionista.getCorreo() + "'," +
                "contrasenia='" + coleccionista.getPassword() + "'," +
                "estado='" + coleccionista.getEstado() + "'," +
                "calificacion=" + coleccionista.getCalificacion() + "," +
                "direccion='" + coleccionista.getDireccion() + "'," +
                "esModerador=" + coleccionista.getModerador() + "" +
                " WHERE userName= '" + coleccionista.getUsername() + "'";
        Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarQuery(query);
        return "Coleccionista actualizada de manera correcta";
    }
    /**
     * metodo de eliminar
     * @param identificacion tipo string que almacena el coleccionista que se va a eliminar
     * @return un coleccionista, mensaje
     * @throws ClassNotFoundException exepcio que lanza una dvertencia que no encuentra los comandos que se le esta asignando.
     * @throws SQLException exepcion tip sql laza una davertencia cuando hay un error en dicho motor
     * @throws Exception exepciones general, padre de todas la exepciones
     */
    @Override
    public String eliminarColeccionista(String identificacion) throws Exception {
        query = "DELETE FROM usuario WHERE userName= '" + identificacion + "'";
        Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarQuery(query);
        return "Coleccionista eliminada de manera correcta";
    }
    /**
     * metodo de buscar
     * @param userName tipo string que almacena el coleccionista que se va a buscar
     * @return un coleccionista
     * @throws ClassNotFoundException exepcio que lanza una dvertencia que no encuentra los comandos que se le esta asignando.
     * @throws SQLException exepcion tip sql laza una davertencia cuando hay un error en dicho motor
     * @throws Exception exepciones general, padre de todas la exepciones
     */
    @Override
    public Coleccionista buscarColeccionista(String userName) throws Exception {
        query = "SELECT * FROM usuario WHERE userName ='" + userName + "'";
        ResultSet rsColeccionista = Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarSQL(query);
        if (rsColeccionista.next()) {
            Coleccionista cole = new Coleccionista(rsColeccionista.getString("identificacion"),
                    rsColeccionista.getString("tipo"), rsColeccionista.getString("nombre"),
                    rsColeccionista.getString("avatar"),
                    LocalDate.parse(rsColeccionista.getString("fechanacimiento")),
                    Integer.parseInt(rsColeccionista.getString("edad")),
                    rsColeccionista.getString("userName"),
                    rsColeccionista.getString("correo"),
                    rsColeccionista.getString("contrasenia"), rsColeccionista.getString("estado"),
                    Double.parseDouble(rsColeccionista.getString("calificacion")),
                    rsColeccionista.getString("direccion"), Integer.parseInt(rsColeccionista.getString("esModerador")));
            return cole;
        }
        return null;
    }


    /**
     * metodo de listar coleccionista
     * @return un  lista de coleccionista
     * @throws ClassNotFoundException exepcio que lanza una dvertencia que no encuentra los comandos que se le esta asignando.
     * @throws SQLException exepcion tip sql laza una davertencia cuando hay un error en dicho motor
     * @throws Exception exepciones general, padre de todas la exepciones
     */
    @Override
    public ArrayList<String> listarColeccionistas() throws Exception {
        query = "SELECT * FROM USUARIO";
        ArrayList<String> lista = new ArrayList<>();
        ResultSet rs = Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarSQL(query);
        while (rs.next()) {
            Coleccionista colec = new Coleccionista(
                    rs.getString("identificacion"),
                    rs.getString("TIPO"),
                    rs.getString("NOMBRE"),
                    rs.getString("AVATAR"),
                    LocalDate.parse(rs.getString("fechaNacimiento")),
                    rs.getInt("EDAD"),
                    rs.getString("userName"),
                    rs.getString("CORREO"),
                    rs.getString("CONTRASENIA"),
                    rs.getString("ESTADO"),
                    rs.getDouble("CALIFICACION"),
                    rs.getString("DIRECCION"),
                    rs.getInt("ESMODERADOR"));
            lista.add(colec.toString());
        }
        return lista;

    }



    /**
     * metodo de asociar obejto coleccionista
     * @return un  lista de coleccionista
     * @throws ClassNotFoundException exepcio que lanza una dvertencia que no encuentra los comandos que se le esta asignando.
     * @throws SQLException exepcion tip sql laza una davertencia cuando hay un error en dicho motor
     * @throws Exception exepciones general, padre de todas la exepciones
     */
    @Override
    public String asociarObjetoColeccionista(String idColeccionista, String codObjeto) throws Exception {
        query = "INSERT INTO ITEM_usuario(usuario_identificacion, ITEM_codigo) VALUES ('" + idColeccionista + "','" + codObjeto + "')";
        Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarQuery(query);
        return "El item con codigo " + codObjeto + " se agregó de forma correcta al coleccionista";
    }

    public String asociarObjetosSubasta(String codSubasta, String codObjeto) throws Exception {
        query = "INSERT INTO ITEM_subasta (subasta_codigo, ITEM_codigo) VALUES ('" + codSubasta + "','" + codObjeto + "')";
        Conector.getConector(Utils.getProperties()[0], Utils.getProperties()[1]).ejecutarSQL(query);
        return "La item: " + codObjeto + " se agregó de forma correcta a la subasta: " + codSubasta;
    }
}
