package Montero.Lizbeth.bl.coleccionista;

import Montero.Lizbeth.bl.usuario.Usuario;

import java.io.FileInputStream;
import java.sql.SQLException;
import java.util.ArrayList;

public interface IColeccionistaDAO {
        /**
         * metodo de la interfaz que oblica a la clase sqlserverColeccionistaDao a implementarlo
         * @param coleccionista tipo coleccionista que almacena el coleccionista que se va a registrar
         * @return un mensaje, coleccionista
         * @throws ClassNotFoundException exepcio que lanza una dvertencia que no encuentra los comandos que se le esta asignando.
         * @throws SQLException exepcion tip sql laza una davertencia cuando hay un error en dicho motor
         * @throws Exception exepciones general, padre de todas la exepciones
         */
        public String registrarColeccionista(Coleccionista coleccionista) throws Exception;

        /**
         * metodo de la interfaz que oblica a la clase sqlserverColeccionistaDao a implementarlo
         * @param coleccionista tipo coleccionista que almacena el coleccionista que se va a modificar
         * @return un coleccinista, mensaje
         * @throws ClassNotFoundException exepcio que lanza una dvertencia que no encuentra los comandos que se le esta asignando.
         * @throws SQLException exepcion tip sql laza una davertencia cuando hay un error en dicho motor
         * @throws Exception exepciones general, padre de todas la exepciones
         */
        public String modificarColeccionista(Coleccionista coleccionista) throws Exception;


        /**
         * metodo de la interfaz que oblica a la clase sqlserverColeccionistaDao a implementarlo
         * @param identificacion tipo string que almacena el coleccionista que se va a eliminar
         * @return un coleccionista, mensaje
         * @throws ClassNotFoundException exepcio que lanza una dvertencia que no encuentra los comandos que se le esta asignando.
         * @throws SQLException exepcion tip sql laza una davertencia cuando hay un error en dicho motor
         * @throws Exception exepciones general, padre de todas la exepciones
         */
        public String eliminarColeccionista(String identificacion) throws Exception;

        /**
         * metodo de la interfaz que oblica a la clase sqlserverColeccionistaDao a implementarlo
         * @param userName tipo string que almacena el coleccionista que se va a buscar
         * @return un coleccionista
         * @throws ClassNotFoundException exepcio que lanza una dvertencia que no encuentra los comandos que se le esta asignando.
         * @throws SQLException exepcion tip sql laza una davertencia cuando hay un error en dicho motor
         * @throws Exception exepciones general, padre de todas la exepciones
         */
        public Coleccionista buscarColeccionista(String userName) throws Exception;

        /**
         * metodo de la interfaz que oblica a la clase sqlserverColeccionistaDao a implementarlo
         * @return un  lista de coleccionista
         * @throws ClassNotFoundException exepcio que lanza una dvertencia que no encuentra los comandos que se le esta asignando.
         * @throws SQLException exepcion tip sql laza una davertencia cuando hay un error en dicho motor
         * @throws Exception exepciones general, padre de todas la exepciones
         */
        public ArrayList<String> listarColeccionistas() throws Exception;

        /**
         * metodo de la interfaz que oblica a la clase sqlserverColeccionistaDao a implementarlo
         * @return un  lista de coleccionista
         * @throws ClassNotFoundException exepcio que lanza una dvertencia que no encuentra los comandos que se le esta asignando.
         * @throws SQLException exepcion tip sql laza una davertencia cuando hay un error en dicho motor
         * @throws Exception exepciones general, padre de todas la exepciones
         */
        public String asociarObjetoColeccionista(String idColeccionista, String codObjeto) throws Exception;

    }

