package Montero.Lizbeth.bl.coleccionista;

import Montero.Lizbeth.bl.interes.Interes;
import Montero.Lizbeth.bl.item.Item;
import Montero.Lizbeth.bl.usuario.Usuario;
import javafx.scene.control.DatePicker;

import java.time.LocalDate;
import java.util.ArrayList;

/**
 * Atributos de la clase Coleccionista
 *
 * @author Lizbeth Montero
 * @version 1.1.0
 * @since 1.0.0
 */

public class Coleccionista extends Usuario {
    private int edad;
    private String avatar, estado, correo, direccion, identificacion, idSubasta;
    private int moderador;
    private double calificacion;
    private LocalDate fechaNacimiento;

   /* private Item itemDue;
    private ArrayList<Interes> intereses;
    private ArrayList<Item> objetos;
*/

    /**
     * contructor por defecto es utilizado para la invocación de las subclases, palabra reservada super que obtiene los metodos del padre
     */


    public Coleccionista() {

    }/**
     * CONTRUSTOR CON TODOS LOS PARAMETROS
     * @param identificacion TIPO STRING  VARIABLE QUE ALMACENA LA IDENTIFICACION DEL COLECCIONISTA
     * @param tipo TIPO STRING VARIABLE QUE ALMACENA  EL TIPO DE USUARIO
     * @param nombre TIPO STRING VARIABLE QUE ALMACENA EL NOMBRE
     * @param username TIPO STRING VARIABLE QUE ALMACENA EL NOMBRE DE USUARIO
     * @param calificacion TIPO STRING VARIABLE QUE ALMACENA LA CALIFICACION
     */

    public Coleccionista(final String nombre, final String username, final String password, final String tipo, final String identificacion, final int moderador, final double calificacion) {
        super(nombre, username, password, tipo);
        this.identificacion = identificacion;
        this.moderador = moderador;
        this.calificacion = calificacion;
    }

    /**
     * CONTRUSTOR CON TODOS LOS PARAMETROS
     * @param identificacion TIPO STRING  VARIABLE QUE ALMACENA LA IDENTIFICACION DEL COLECCIONISTA
     * @param tipo TIPO STRING VARIABLE QUE ALMACENA  EL TIPO DE USUARIO
     * @param nombre TIPO STRING VARIABLE QUE ALMACENA EL NOMBRE
     * @param avatar TIPO STRING VARIABLE QUE ALMACENA  EL AVATAR
     * @param fechaNacimiento TIPO LOCALDATE VARIABLE QUE ALMACENA  LA FECHA DE NACIMIENTO
     * @param edad TIPO STRING VARIABLE QUE ALMACENA LA EDAD
     * @param username TIPO STRING VARIABLE QUE ALMACENA EL NOMBRE DE USUARIO
     * @param correo TIPO STRING VARIABLE QUE ALMACENA  ALMACENA EL CORREO
     * @param contrasenia TIPO STRING VARIABLE QUE ALMACENA  LA CONTRASENIA
     * @param estado TIPO STRING VARIABLE QUE ALMACENA EL ESTADO
     * @param calificacion TIPO STRING VARIABLE QUE ALMACENA LA CALIFICACION
     * @param direccion TIPO STRING VARIABLE QUE ALMACENA  LA DIRECCION
     * @param esmoderador TIPO BOOLEAN VARIABLE QUE ALMACENA EL MODERADOR
     */
    public Coleccionista(String identificacion, String tipo, String nombre, String avatar, LocalDate fechaNacimiento, int edad, String username, String correo, String contrasenia, String estado, double calificacion, String direccion, int esmoderador) {
        super(nombre, username, contrasenia, tipo);
        this.edad = edad;
        this.avatar = avatar;
        this.estado = estado;
        this.correo = correo;
        this.direccion = direccion;
        this.identificacion = identificacion;
        this.moderador = moderador;
        this.calificacion = calificacion;
        this.fechaNacimiento = fechaNacimiento;
    }

    /**
     * CONTRUCTOR CON UN UNICO APARMETRO
     * @param username VARIABLE QUE ALMACENA EL NOMBRE DE USUARIO
     */
    public Coleccionista(String username) {
        super("", username, "", "");

    }
// Constructor buscar coleccionista

    /**
     * CONTRUSTOR CON TODOS LOS PARAMETROS
     * @param identificacion TIPO STRING  VARIABLE QUE ALMACENA LA IDENTIFICACION DEL COLECCIONISTA
     * @param nombre TIPO STRING VARIABLE QUE ALMACENA EL NOMBRE
     * @param correo TIPO STRING VARIABLE QUE ALMACENA  ALMACENA EL CORREO
     * @param contrasenia TIPO STRING VARIABLE QUE ALMACENA  LA CONTRASENIA
     * @param estado TIPO STRING VARIABLE QUE ALMACENA EL ESTADO
     * @param calificacion TIPO STRING VARIABLE QUE ALMACENA LA CALIFICACION
     * @param direccion TIPO STRING VARIABLE QUE ALMACENA  LA DIRECCION
     */
    public Coleccionista(String identificacion, String nombre, String correo, String contrasenia, String estado, String direccion, double calificacion) {
    }


/**
 * Metodo Getter de la variable que almacena si es moderador, es utilizado para obtener informacion del atributo privado de la clase.
 *
 * @return devuelve si es moderador y , lo hace publico.
 */

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param moderador: dato que almacena si es moderador o no  de manera privada en su calse respectiva.
     */

    /**
     * Metodo Getter de la variable que almacena la identificacion, es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve la identificacion y , lo hace publico.
     */
    public String getIdentificacion() {
        return identificacion;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param identificacion: dato que almacena  la identificacion  manera privada en su calse respectiva.
     */
    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    /**
     * Metodo Getter de la variable que almacena la edad, es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve la edad del coleccionista y , lo hace publico.
     */
    public int getEdad() {
        return edad;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param edad: dato que almacena  la edad  manera privada en su calse respectiva.
     */
    public void setEdad(int edad) {
        this.edad = edad;
    }

    /**
     * Metodo Getter de la variable que almacena el nombre del coleccionista, es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve la el nombre del coleccionista y , lo hace publico.
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param nombre: dato que almacena el nombre  manera privada en su calse respectiva.
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * Metodo Getter de la variable que almacena el avatar del coleccionista, es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve la el avatar del coleccionista y , lo hace publico.
     */
    public String getAvatar() {
        return avatar;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param avatar: dato que almacena el avatar  manera privada en su calse respectiva.
     */
    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    /**
     * Metodo Getter de la variable que almacena el estado del coleccionista, es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve la el estado del coleccionista y , lo hace publico.
     */
    public String getEstado() {
        return estado;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param estado: dato que almacena el estado  manera privada en su calse respectiva.
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }

    /**
     * Metodo Getter de la variable que almacena el correo del coleccionista, es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve la el correo del coleccionista y , lo hace publico.
     */
    public String getCorreo() {
        return correo;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param correo: dato que almacena el correo  manera privada en su calse respectiva.
     */
    public void setCorreo(String correo) {
        this.correo = correo;
    }

    /**
     * Metodo Getter de la variable que almacena la direccion del coleccionista, es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve la direccion del coleccionista y , lo hace publico.
     */
    public String getDireccion() {
        return direccion;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param direccion: dato que almacena la direccion  manera privada en su calse respectiva.
     */
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    /**
     * Metodo Getter de la variable que almacena la contrasennia del coleccionista, es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve la contrasennia del coleccionista y , lo hace publico.
     */
    public String getPassword() {
        return password;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param password: dato que almacena la password  manera privada en su calse respectiva.
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Metodo Getter de la variable que almacena la calificacion del coleccionista, es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve la calificacion del coleccionista y , lo hace publico.
     */
    public double getCalificacion() {
        return calificacion;
    }
    /**
     * Metodo Getter de la variable que almacena el item  del coleccionista, es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve el item del coleccionista y , lo hace publico.
     */

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param calificacion: dato que almacena la calificacion  manera privada en su calse respectiva.
     */
    public void setCalificacion(double calificacion) {
        this.calificacion = calificacion;
    }

    /**
     * Metodo Getter de la variable que almacena la fecha de nacimiento   del coleccionista, es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve la fecha de nacimiento del coleccionista y , lo hace publico.
     */
    public LocalDate getFechaNacimiento() {
        return fechaNacimiento;
    }

    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param fechaNacimiento: dato que almacena la fechaNacimiento  manera privada en su calse respectiva.
     */
    public void setFechaNacimiento(LocalDate fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }
    /**
     * Metodo Getter de la variable que almacena si es moderador  del coleccionista, es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve el moderador del coleccionista y , lo hace publico.
     */
    public int getModerador() {
        return this.moderador;
    }
    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param moderador: dato que almacena el moderador  manera privada en su calse respectiva.
     */

    public void setModerador(final int moderador) {
        this.moderador = moderador;
    }
    /**
     * Metodo Getter de la variable que almacena el tipo de usuario  del coleccionista, es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve el tipo de usuario  y , lo hace publico.
     */
    public String getTipo() {
        return tipo;
    }
    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param tipo: dato que almacena el tipo  manera privada en su calse respectiva.
     */
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    /**
     * Metodo Getter de la variable que almacena el id de la subasta, es utilizado para obtener informacion del atributo privado de la clase.
     *
     * @return devuelve el tipo de usuario  y , lo hace publico.
     */
    public String getIdSubasta() {
        return idSubasta;
    }
    /**
     * Metodo set,es utilizado para asignar valores a los atributos privados de una clase.
     *
     * @param idSubasta: dato que almacena el ide de la subasta  manera privada en su calse respectiva.
     */
    public void setIdSubasta(String idSubasta) {
        this.idSubasta = idSubasta;
    }

    @Override
    public String toString() {
        return "Coleccionista{" +   ", identificacion='" + identificacion
                +", username='" + username  +", nombre='" + nombre +
                ", correo='" + correo + '\'' + ", password='" + password + '\''
                + "edad=" + edad + ", avatar='" + avatar +
                ", estado='" + estado + '\'' + ", moderador=" + moderador +
                ", calificacion=" + calificacion +
                ", fechaNacimiento=" + fechaNacimiento +
                ", direccion='" + direccion + '\'' +
                '}';
    }

    /**
     * Metodo toString utilizado para imprimir la in información almacenada en un String asignado.
     *
     * @return retorna los valores respectivos del coleccionista, en este caso el respectivo valor de cada atributo del administrador en un solo String.
     */
    public String toStringCSV() {
        return idenitificacion + "," + tipo + "," + "," + nombre + "," + avatar + ","
                + fechaNacimiento + "," + edad + "," + correo + "," + password + "," + estado + "," +
                calificacion + "," + direccion + "," + moderador;
    }
}
